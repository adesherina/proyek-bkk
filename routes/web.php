<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//====================================ADMIN===================================
//dashboard
Route::get('/dashboard', function () {
    if (session('berhasil_login')) {
        return view('Admin.dashboard');
    } else {
        return redirect('/login');
    }
});

//login
Route::get('admin/login', 'admin\LoginController@index')->name('login');
Route::post('adminlogin', 'admin\LoginController@login')->name('login');

//logout
Route::get('logout', 'admin\LoginController@logout')->name('logout');

//register
Route::get('admin/register', 'admin\RegisterController@index');
Route::post('register/add', 'admin\RegisterController@addProcess')->name('register');

//forgot password
Route::get('admin/forgot', 'admin\forgotPassword@index');
Route::post('admin/forgot/noTelp', 'admin\forgotPassword@tampil');
Route::patch('admin/forgot/pw', 'admin\forgotPassword@forgotPw');

//profil
Route::get('admin/profil/{id}', 'admin\ProfilController@index')->name('admin.update')->middleware('CheckLoginAdmin');
Route::patch('admin/{id}', 'admin\ProfilController@updateProcess');

//update password
Route::get('admin/pw/{id}', 'admin\ProfilController@updatePw')->name('admin.update.pw')->middleware('CheckLoginAdmin');
Route::patch('admin/pw/ubah/{id}', 'admin\ProfilController@updatePassword')->name('admin.updatePassword');

//Admin
Route::get('dataadmin', 'admin\AdminController@data')->middleware('CheckLoginAdmin');
Route::get('dataadmin/add', 'admin\AdminController@add');
Route::post('dataadmin', 'admin\AdminController@addProcess')->name('admin.simpandata');
Route::get('dataadmin/update/{id}', 'admin\AdminController@update')->name('admin.update');
Route::patch('dataadmin/{id}', 'admin\AdminController@updateProcess')->name('admin.updateProcess');
Route::delete('dataadmin/{id}', 'admin\AdminController@delete');

//User
Route::get('datauser', 'admin\UserAdminController@data')->middleware('CheckLoginAdmin');
Route::delete('datauser/delete/{id}', 'admin\UserAdminController@delete');
Route::get('datauser/{id}/edit', 'admin\UserAdminController@edit')->name('user.editdata');
Route::patch('datauser/{id}', 'admin\UserAdminController@update')->name('user.update');
Route::delete('datauser/delete/{id}', 'admin\UserAdminController@delete')->name('user.deletedata');
Route::get('datauser/{id}/detail', 'admin\UserAdminController@detail')->name('user.detaildata');

//Perusahaan
Route::get('pt/list', 'admin\perusahaanController@index')->middleware('CheckLoginAdmin');
Route::get('pt/Tambah', 'admin\perusahaanController@tambah')->name('pt.tambahdata');
Route::post('pt/Simpan', 'admin\perusahaanController@simpan')->name('pt.simpandata');
Route::get('pt/{id}/edit', 'admin\perusahaanController@edit')->name('pt.editdata');
Route::patch('pt/{id}', 'admin\perusahaanController@update')->name('pt.update');
Route::delete('pt/delete/{id}', 'admin\perusahaanController@delete')->name('pt.deletedata');
Route::get('pt/{id}/detail', 'admin\perusahaanController@detail')->name('pt.detaildata');

//bidang
Route::get('bidang/list', 'admin\BidangController@index')->name('bidang.index');
Route::get('bidang/create', 'admin\BidangController@create')->name('bidang.create');
Route::post('bidang/create', 'admin\BidangController@save')->name('bidang.post');
Route::get('bidang/{id}/edit', 'admin\BidangController@edit')->name('bidang.editdata');
Route::patch('bidang/{id}', 'admin\BidangController@update')->name('bidang.update');
Route::delete('bidang/delete/{id}', 'admin\BidangController@delete')->name('bidang.deletedata');

//loker
Route::get('loker/list', 'admin\lokerController@index')->middleware('CheckLoginAdmin');
Route::get('loker/Tambah', 'admin\lokerController@tambah')->name('loker.tambahdata');
Route::post('loker/Simpan', 'admin\lokerController@simpan')->name('loker.simpandata');
Route::get('loker/{id}/edit', 'admin\lokerController@edit')->name('loker.editdata');
Route::patch('loker/{id}', 'admin\lokerController@update')->name('loker.update');
Route::delete('loker/delete/{id}', 'admin\lokerController@delete')->name('loker.deletedata');
Route::get('loker/{id}/detail', 'admin\lokerController@detail')->name('loker.detaildata');

//Pendaftaran
Route::get('pendaftaran/list', 'admin\pendaftaranController@index')->middleware('CheckLoginAdmin');
Route::get('pendaftaran/{id}/detail', 'admin\pendaftaranController@detail')->name('pendaftaran.detaildata');
Route::get('pendaftaran/berkas-pribadi/download/{id}', 'admin\pendaftaranController@downloadPribadi')->name('berkas.pribadi.download')->middleware('CheckLoginAdmin');
Route::get('pendaftaran/berkas-persyaratan/download/{id}', 'admin\pendaftaranController@downloadPersyaratan')->name('berkas.persyaratan.download')->middleware('CheckLoginAdmin');
Route::get('pendaftaran/lunas/{id}', 'admin\pendaftaranController@makeLunas')->name('pendaftaran.lunas')->middleware('CheckLoginAdmin');
Route::get('pendaftaran/pending/{id}', 'admin\pendaftaranController@makePending')->name('pendaftaran.pending')->middleware('CheckLoginAdmin');
Route::post('pendaftaran/ubah-status', 'admin\pendaftaranController@changeStatus')->name('pendaftaran.change.status')->middleware('CheckLoginAdmin');

//timeline
Route::get('timeline/list', 'admin\timelineController@index')->middleware('CheckLoginAdmin');
Route::get('timeline/Tambah', 'admin\timelineController@tambah')->name('timeline.tambahdata');
Route::post('timeline/Simpan', 'admin\timelineController@simpan')->name('timeline.simpandata');
Route::get('timeline/{id}/edit', 'admin\timelineController@edit')->name('timeline.editdata');
Route::patch('timeline/{id}', 'admin\timelineController@update')->name('timeline.update');
Route::delete('timeline/delete/{id}', 'admin\timelineController@delete')->name('timeline.deletedata');
Route::get('timeline/{id}/detail', 'admin\timelineController@detail')->name('timeline.detaildata');

//Email Konfigurasi
Route::get('email/list', 'admin\emailKonfigController@index')->middleware('CheckLoginAdmin');
Route::get('email/Tambah', 'admin\emailKonfigController@tambah')->name('email.tambahdata');
Route::post('email/Simpan', 'admin\emailKonfigController@simpan')->name('email.simpandata');
Route::get('email/{id}/edit', 'admin\emailKonfigController@edit')->name('email.editdata');
Route::patch('email/{id}', 'admin\emailKonfigController@update')->name('email.update');
Route::delete('email/delete/{id}', 'admin\emailKonfigController@delete')->name('email.deletedata');


// ==========================================USER====================================================


//beranda
Route::get('/', 'user\berandaController@beranda');

//lowongan
Route::get('lowongan', 'user\lowonganController@lowongan');

//profil bkk
Route::get('profil', 'user\profilController@profil');
Route::get('kontak', 'user\profilController@kontak');

//mitra
Route::group(['prefix' => 'mitra', 'namespace' => 'user'], function () {
    Route::get('/', 'mitraController@index');
    Route::get('/sort/{value}', 'mitraController@sortBy');
    Route::post('/search', 'mitraController@searchFunc')->name('mitra.search');
    Route::get('/detail/{id}', 'mitraController@detail')->name('mitra.detail');
});

Route::group(['prefix' => 'lowongan', 'namespace' => 'user'], function () {
    Route::get('/', 'lowonganController@lowongan');
    Route::post('/search', 'lowonganController@searchFunc')->name('lowongan.search');
    Route::get('/detail/{id}', 'lowonganController@detail')->name('lowongan.detail');
    Route::get('/berkas-persyaratan/download/{id}', 'lowonganController@download')->name('persyaratan.download');
});


//user
Route::group(['prefix' => 'user', 'namespace' => 'user'], function () {
    Route::get('/register', 'registerController@index')->name('user.register');
    Route::post('/register/add', 'registerController@addProcess');
    Route::get('/login', 'loginController@index')->name('user.login');
    Route::post('/login/proses', 'loginController@login')->name('user.login.proses');
    Route::get('/logout', 'loginController@logout')->name('user.logout');
    Route::get('/verification/{token}', 'registerController@verification')->name('user.logout');
});

Route::group(['prefix' => 'pendaftaran', 'namespace' => 'user'], function () {
    Route::post('/save', 'PendaftaranController@save')->name('user.pendaftaran.save');
    Route::get('/confirmation/{id}', 'PendaftaranController@confirmation')->name('user.pendaftaran.confirmation');
    Route::post('/upload-file', 'PendaftaranController@uploadFile')->name('user.pendaftaran.upload');
});

//google
Route::get('google', 'user\GoogleController@redirect');
Route::get('callback/google', 'user\GoogleController@callbackHandle');

//verifikasi email
Auth::routes();

//profil
Route::get('user/profil/{id}', 'user\profilSayaController@update')->name('user.update');
Route::patch('user/profil/update/{id}', 'user\profilSayaController@updateProcess')->name('user.updateProcess');

//update password
Route::get('user/pw/{id}', 'user\profilSayaController@updatePw')->name('user.update.pw');
Route::patch('user/pw/update/{id}', 'user\profilSayaController@updatePassword')->name('user.updatePassword');

Route::get('user/lamaran', 'user\profilSayaController@listLamaran')->name('user.daftar.lamaran');
Route::get('user/lamaran/{status}', 'user\profilSayaController@statusLamaran')->name('user.status.lamaran');

//forgot password
Route::get('user/forgotpw/tampil', 'user\forgotPassword@index')->name('forgotpw');
Route::post('user/forgotpw/cariEmail', 'user\forgotPassword@cariEmail')->name('forgotpw2');
Route::patch('user/forgotpw/ubah', 'user\forgotPassword@forgotPw')->name('forgotpw3');
