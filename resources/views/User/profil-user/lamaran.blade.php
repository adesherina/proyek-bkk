<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
        <meta name="csrf-token" content="{{ csrf_token() }}">
   </head>

   <body>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>

        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('assets/user/img/4565.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Histori Lamaran Pekerjaan Saya</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End -->
        <!-- Job List Area Start -->
        <div class="job-listing-area pt-120 pb-120">
            <div class="container">
                <div class="row">
                    <!-- Right content -->
                    <div class="col-xl-9 col-lg-9 col-md-8">
                        <!-- Featured_job_start -->
                        <section class="featured-job-area">
                            <div class="container">
                                <!-- Count of Job list Start -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="count-job mb-35">
                                            <span>{{$lm->count()}} Lamaran Saya</span>
                                            <!-- Select job items start -->
                                            <div class="select-job-items">
                                                <span>Sort by</span>
                                                <select name="select" id="status">
                                                    <option value="all">None</option>
                                                    <option value="verified">Diterima</option>
                                                    <option value="unverified">Menunggu</option>
                                                    <option value="reject">Ditolak</option>
                                                </select>
                                            </div>
                                            <!--  Select job items End-->
                                        </div>
                                    </div>
                                </div>
                                <!-- Count of Job list End -->
                                <!-- single-job-content -->
                                <div id="lamaran">
                                    @foreach ($lm as $item)
                                        <div class="single-job-items mb-30">
                                            <div class="job-items">
                                                <div class="company-img">
                                                    <a href="#"><img src="{{url('foto-PT/'. $item->lokerRef->perusahaan->photo)}}" style="max-width: 70px; 7eight:60px" alt=""></a>
                                                </div>
                                                <div class="job-tittle job-tittle2">
                                                    <a href="{{route('lowongan.detail', $item->lokerRef->id)}}">
                                                        <h4>{{$item->lokerRef->nama_loker}} @if($item->registrasi === "lunas") <i class="fas fa-check-circle ml-1"></i> @endif</h4>
                                                    </a>
                                                    <ul>
                                                        <li>{{$item->lokerRef->perusahaan->nama_pt}}</li>
                                                        <li>{{$item->lokerRef->range_gaji}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="items-link items-link2 f-right">
                                                <a href="#" href="javascript:void(0);">
                                                    @if ($item->status === "verified")
                                                        Diterima
                                                    @elseif($item->status === "unverified")
                                                        Proses Cek
                                                    @else
                                                        Tidak Diterima
                                                    @endif
                                                </a>
                                                <span>update {{$item->updated_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                        <!-- Featured_job_end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Job List Area End -->
        <!--Pagination Start  -->

        <!--Pagination End  -->

    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')
		
    <script>
        $('#status').on('change', function(){
            var status = $(this).val();
            if(status != null){
                $.ajax({
                    url: "/user/lamaran/"+status,
                    type: "GET",
                    success: function (data) {
                        console.log(data);
                        document.getElementById('lamaran').innerHTML = data;
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    </script>
        
    </body>
</html>