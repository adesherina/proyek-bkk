<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>
        <div class="slider-area ">
        <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('assets/user/img/4565.jpg')}}">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Profil Saya</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Hero Area End -->
    <!-- ================ contact section start ================= -->
    <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        @if (session('statusSuccess1'))
                            <div class="alert alert-success mt-3">
                                {{ session('statusSuccess1') }}
                            </div>
                        @elseif(session('statusSuccess2'))
                            <div class="alert alert-success mt-3">
                                {{ session('statusSuccess2') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <img src="{{url('user-profil/'. $user->photo)}}" class="img-thumbnail" style="width: 200px;height:200px; border-radius:50%;border:2px solid #9647ae" alt="">
                    </div>
                    <div class="col-lg-8">
                        {{-- <h2 class="contact-title">Profil Saya</h2> --}}
                        <form class="form-contact contact_form" action="{{url('user/profil/update/' .$user->id)}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            @method('patch')
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input class="form-control mt-2" name="nama_lengkap" type="text" value="{{$user->nama_lengkap}}" placeholder="Nama Lengap Anda">
                                    </div>
                                     <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control mt-2" name="email"  type="text" value="{{ $user->email }}" placeholder="Email Anda">
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <textarea class="form-control mt-2" placeholder="Alamat Anda" name="alamat" value="{{ $user->alamat }}" required>{{ $user->alamat }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault1" <?php if($user->jenis_kelamin == "Laki-laki"){ ?> checked <?php } ?> value="Perempuan">
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Laki - Laki
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="flexRadioDefault2" <?php if($user->jenis_kelamin == "Perempuan"){ ?> checked <?php } ?>  value="Perempuan">
                                            <label class="form-check-label" for="flexRadioDefault2">
                                                Perempuan
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tb">Foto</label>
                                        <input class="form-control mt-2" name="photo"  type="file" value="{{ $user->photo }}" >
                                        <input type="hidden" name="oldPhoto" value="{{$user->photo}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')
		
        
    </body>
</html>