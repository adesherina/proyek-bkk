<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>
        <div class="slider-area ">
        <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('assets/user/img/4565.jpg')}}">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Login</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Hero Area End -->
    <!-- ================ contact section start ================= -->
    <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @elseif(session('message2'))
                            <div class="alert alert-danger">
                                {{ session('message2') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Login</h2>
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact contact_form" action="{{ route('user.login.proses') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control mt-2" name="email"type="text"  placeholder="Username Anda">
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="float-left">Password</label>
                                        <a href="{{url('user/forgotpw/tampil')}}" class="float-right " style="color:#9e32a9;">Lupa Password ?</a>
                                        <input class="form-control mt-2" name="password" type="password" placeholder="Password Anda">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn mr-5">Login</button>
                                <a href="{{url('google') }}" class="button button-contactForm boxed-btn">Login with Google</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')
		
        
    </body>
</html>