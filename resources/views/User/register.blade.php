<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>
        <div class="slider-area ">
        <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('assets/user/img/4565.jpg')}}">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Register</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Hero Area End -->
    <!-- ================ contact section start ================= -->
    <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Register</h2>
                        @if (session()->has('statusRegister'))
                            <div class="alert alert-success">
                                {{ Session::get('statusRegister') }}
                            </div>
                        @elseif(session()->has('statusFailed'))
                         <div class="alert alert-danger">
                                {{ Session::get('statusFailed') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact contact_form" action="{{url('user/register/add')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input class="form-control mt-2" name="nama_lengkap" type="text" value="{{ old('nama_lengkap') }}" placeholder="Nama Lengap Anda">
                                        @if ($errors->has('nama_lengkap'))
                                            <span class="text-danger">{{ $errors->first('nama_lengkap') }}</span>
                                        @endif
                                    </div>
                                     <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control mt-2" name="email"  type="text" value="{{ old('email') }}" placeholder="Email Anda">
                                        @if ($errors->has('email'))
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input class="form-control mt-2" name="password" type="password" placeholder="Password Anda">
                                        @if ($errors->has('password'))
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmPassword">Konfirmasi password</label>
                                        <input class="form-control mt-2" name="confirmPassword" type="password" placeholder="Konfirmasi Password Anda">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Register</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </section>
    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')
		
        
    </body>
</html>