<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>
        <div class="slider-area ">
        <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('assets/user/img/4565.jpg')}}">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Forgot Password</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Hero Area End -->
    <!-- ================ contact section start ================= -->
    <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        @if (session('statusSuccess1'))
                            <div class="alert alert-success mt-3">
                                {{ session('statusSuccess1') }}
                            </div>
                        @elseif(session('statusSuccess2'))
                            <div class="alert alert-success mt-3">
                                {{ session('statusSuccess2') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-8">
                        {{-- <h2 class="contact-title">Profil Saya</h2> --}}
                        <form class="form-contact contact_form" action="{{url('user/forgotpw/cariEmail')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control mt-2" name="email" type="text" placeholder="Email Anda">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')
		
        
    </body>
</html>