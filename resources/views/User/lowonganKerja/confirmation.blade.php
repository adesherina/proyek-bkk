<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    {{-- <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>

        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('assets/user/img/banner/banner-cuy.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center text-white">
                                <h2>{{$loker->lokerRef->nama_loker}}</h2>
                                <h4 class="text-white">{{$loker->lokerRef->perusahaan->nama_pt}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="job-listing-area pt-80 pb-120">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="card mx-auto shadow" style="width: 40%; border-radius:50px; border-bottom:9px solid #9E32A9">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <img src="{{url('icon/confirmation.svg')}}" style="width: 300px; height:300px;" alt="">
                                    </div>
                                </div>
                                <div class="row mt-5 mb-5 pl-2">
                                    <div class="col-12">
                                        <a href="#" class="detail_loker-btn" onclick="openModal()" type="button"  style="left: 50%;">Upload Berkas Pendaftaran</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Job List Area End -->

        <div class="modal fade" id="exampleModalCenter" style="border-radius:20px" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="mr-2 mt-2">
                    <a href="#" onclick="closeModal()" class="close close-icon float-right"></a>
                </div>
                <div class="upload-title ml-3 mt-3">
                  <h5 class="modal-title" id="exampleModalLongTitle">Upload Berkas Pribadi & Persyaratan</h5>
                </div>
                <div class="modal-body">
                    <div class="row mt-3">
                        <div class="col-6">
                            <p class="font-weight-bold">Berkas Pribadi</p>
                        </div>
                        <div class="col-6">
                            <a href="#" class="detail_loker-btn" onclick="uploadFile('dokumen_pribadi')">Upload</a>
                        </div>
                        <div class="col-12">
                            <div style="width:40%; border-bottom:1px solid #eceff8; " >
                                <p style="margin:0" class="text-secondary" id="dokumen_pribadi_text">ekstensi dalam .zip, .rar</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row mt-3">
                        <div class="col-6">
                            <p class="font-weight-bold">Berkas Persyaratan</p>
                        </div>
                        <div class="col-6">
                            <a href="#" class="detail_loker-btn" onclick="uploadFile('dokumen_persyaratan')">Upload</a>
                        </div>
                        <div class="col-12">
                            <div style="width:40%; border-bottom:1px solid #eceff8; " >
                                <p style="margin:0" class="text-secondary" id="dokumen_persyaratan_text">ekstensi dalam .zip, .rar</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="{{route('user.pendaftaran.upload')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="pendaftaran_id" id="inp_id">
                        <input type="file" class="d-none" onchange="previewFile('dokumen_pribadi');" id="inp_dokumen_pribadi" name="dokumen_pribadi">
                        <input type="file" class="d-none" onchange="previewFile('dokumen_persyaratan');" id="inp_dokumen_persyaratan" name="dokumen_persyaratan">
                        <button type="button" class="btn head-btn2" style="padding: 20px 20px;" data-dismiss="modal">Close</button>
                        <input value="Save changes" type="submit" class="btn head-btn1"  style="padding: 20px 20px;">
                    </form>
                </div>
              </div>
            </div>
          </div>
        <!--Pagination Start  -->
        
        <!--Pagination End  -->

    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')

    @if (count($errors) > 0)
    <script type="text/javascript">
        $( document ).ready(function() {
             $('#form-pendaftaran').collapse('show');
        });
    </script>
   @endif

    <script>
        function openModal(){
            event.preventDefault();
            $('#exampleModalCenter').modal('show');
            var id = "{{ $loker->id}}";
            $('#inp_id').val(id)
        }
        function closeModal(){
            event.preventDefault();
            $('#exampleModalCenter').modal('hide');
        }

        function uploadFile(value){
                event.preventDefault();
                $("#inp_"+value).trigger('click');
                return false;
        }

        function previewFile(value){
            var file = $('#inp_'+value)[0].files[0].name;
            $('#'+value+"_text").text(file);
        }
    </script>
    </body>
</html>