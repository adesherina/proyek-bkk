<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>

        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('assets/user/img/4565.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Dapatkan Pekerjaan Anda</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End -->
        <!-- Job List Area Start -->
        <div class="job-listing-area pt-120 pb-120">
            <div class="container">
                <div class="row">
                    <!-- Left content -->
                    <div class="col-xl-3 col-lg-3 col-md-4">
                        <div class="row">
                            <div class="col-12">
                                    <div class="small-section-tittle2 mb-45">
                                    <div class="ion"> <svg 
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="20px" height="12px">
                                    <path fill-rule="evenodd"  fill="rgb(27, 207, 107)"
                                        d="M7.778,12.000 L12.222,12.000 L12.222,10.000 L7.778,10.000 L7.778,12.000 ZM-0.000,-0.000 L-0.000,2.000 L20.000,2.000 L20.000,-0.000 L-0.000,-0.000 ZM3.333,7.000 L16.667,7.000 L16.667,5.000 L3.333,5.000 L3.333,7.000 Z"/>
                                    </svg>
                                    </div>
                                    <h4>Filter Jobs</h4>
                                </div>
                            </div>
                        </div>
                        <!-- Job Category Listing start -->
                        <div class="job-category-listing mb-50">
                            <!-- single one -->
                            <form action="{{route('lowongan.search')}}" method="POST">
                                @csrf
                                <div class="single-listing">
                                    <div class="small-section-tittle2">
                                        <h4>Nama Pekerjaan</h4>
                                    </div>
                                    <div class="input-form mb-4">
                                        <input type="text" class="form-control" @isset($keyword) value="{{$keyword}}" @endisset name="name" placeholder="Cari Lowongan">
                                    </div>
                                    <div class="small-section-tittle2">
                                        <h4>Bidang Pekerjaan</h4>
                                    </div>
                                    <!-- Select job items start -->
                                    <div class="select-job-items2">
                                        <select name="bidang">
                                            @isset($bidangFilter) 
                                                <option value="{{$bidangFilter->id}}">{{$bidangFilter->nama_bidang}}</option>
                                            @endisset
                                            <option value="null" selected>Semua Bidang</option>
                                            @foreach ($bidang as $bidang)
                                            <option value="{{$bidang->id}}">{{$bidang->nama_bidang}}</option> 
                                            @endforeach
                                        </select>
                                    </div>
                                    <!--  Select job items End-->
                                    <!-- select-Categories start -->
                                    <div class="select-Categories pt-80 pb-50">
                                        <div class="small-section-tittle2">
                                            <h4>Status Lowongan</h4>
                                        </div>
                                        <label class="container">Dibuka
                                            <input name="status" @isset($status) @if($status === "open") checked @endif @endisset value="open" type="checkbox" >
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Ditutup
                                            <input name="status" value="closed"  @isset($status) @if($status === "closed") checked @endif @endisset type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <!-- select-Categories End -->
                                </div>
                                <div class="single-listing">
                                    <!-- Range Slider Start -->
                                    <aside class="left_widgets p_filter_widgets price_rangs_aside sidebar_box_shadow">
                                        <div class="small-section-tittle2">
                                            <h4>Filter Jobs</h4>
                                        </div>
                                        <div class="widgets_inner">
                                            <div class="range_item">
                                                <!-- <div id="slider-range"></div> -->
                                                <input type="text" class="js-range-slider" value="" />
                                                <div class="d-flex align-items-center">
                                                    <div class="price_text">
                                                        <p>Kuota :</p>
                                                    </div>
                                                    <div class="price_value d-flex justify-content-center">
                                                        <input type="text" @isset($minKuota) value="{{$minKuota}}" @endisset name="min" class="js-input-from" id="amount" readonly />
                                                        <span>to</span>
                                                        <input type="text" name="max" @isset($maxKuota) value="{{$maxKuota}}" @endisset class="js-input-to" id="amount" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                <!-- Range Slider End -->
                                </div>
                                <input type="submit" class="btn head-btn1 mt-3" style="padding: 20px 30px" value="Cari">
                            </form>
                        </div>
                        <!-- Job Category Listing End -->
                    </div>
                    <!-- Right content -->
                    <div class="col-xl-9 col-lg-9 col-md-8">
                        <!-- Featured_job_start -->
                        <section class="featured-job-area">
                            <div class="container">
                                <!-- Count of Job list Start -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="count-job mb-35">
                                            <span>{{$loker->count()}} Lowongan Ditemukan</span>
                                            <!-- Select job items start -->
                                            <div class="select-job-items">
                                                <span>Sort by</span>
                                                <select name="select">
                                                    <option value="">None</option>
                                                    <option value="">job list</option>
                                                    <option value="">job list</option>
                                                    <option value="">job list</option>
                                                    
                                                </select>
                                            </div>
                                            <!--  Select job items End-->
                                        </div>
                                    </div>
                                </div>
                                <!-- Count of Job list End -->
                                <!-- single-job-content -->
                                @foreach ($loker as $item)
                                    <div class="single-job-items mb-30">
                                        <div class="job-items">
                                            <div class="company-img">
                                                <a href="#"><img src="{{url('foto-PT/'. $item->perusahaan->photo)}}" style="max-width: 70px; 7eight:60px" alt=""></a>
                                            </div>
                                            <div class="job-tittle job-tittle2">
                                                <a href="{{route('lowongan.detail', $item->id)}}">
                                                    <h4>{{$item->nama_loker}}</h4>
                                                </a> 
                                                <ul>
                                                    <li>{{$item->perusahaan->nama_pt}}</li>
                                                    <li>{{$item->range_gaji}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="items-link items-link2 f-right">
                                            <a href="#" href="javascript:void(0);">{{$item->status}}</a>
                                            <span>{{$item->created_at->diffForHumans() }}</span>
                                        </div> 
                                    </div>
                                @endforeach
                                <div class="pagination-area pb-115 text-center">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="single-wrap d-flex justify-content-center">
                                                    <nav aria-label="Page navigation example">
                                                        {{ $loker->links('vendor.pagination.custom') }}
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Featured_job_end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Job List Area End -->
        <!--Pagination Start  -->

        <!--Pagination End  -->

    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')
		
        
    </body>
</html>