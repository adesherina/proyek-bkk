<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    {{-- <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>

        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('banner-PT/'.$loker->perusahaan->gambar_banner)}}">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center text-white">
                                <h2>{{$loker->nama_loker}}</h2>
                                <h4 class="text-white">{{$loker->perusahaan->nama_pt}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="job-listing-area pt-80 pb-120">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block" style="background-color: #7fad39">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                                <strong class="text-white">{{ $message }}</strong>
                            </div>
                        @endif
          
                        @if ($message = Session::get('failed'))
                            <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row mb-5 d-none" id="timeline">
                    <div class="col-lg-12">
                        {{-- <div  style="display:inline-block;width:100%;overflow-y:auto; height:500px; padding-top:100px"> --}}
                        <div style="height: auto;width:100%;overflow-x:scroll;padding-top:100px">
                            <ul class="timeline timeline-horizontal">
                                @foreach ($tl as $tl)
                                    <li class="timeline-item">
                                        <div class="timeline-badge primary"><i class="fas fa-calendar-alt"></i></div>
                                        <div class="timeline-panel">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title">{{$loker->nama_loker}}</h4>
                                                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> start on {{$tl->tanggal_pembukaan}} </small></p>
                                            </div>
                                            <div class="timeline-body">
                                                {!!$tl->deskripsi!!}
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        {{-- </div> --}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="card shadow-sm">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-3 col-12">
                                                <img src="{{url('foto-PT/'. $loker->perusahaan->photo)}}" class="company_image_loker img-thumbnail" alt="">
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="loker-title-detail">
                                                    <p class="h5 font-weight-bold">{{$loker->nama_loker}} - {{$loker->perusahaan->nama_pt}}</p>
                                                </div>
                                                <div class="loker-created">
                                                    <p>Tanggal Disposting : {{$loker->created_at->isoFormat('D MMMM Y')}}</p>
                                                </div>
                                                <div class="loker-salary" style="margin-top: -15px">
                                                    <p>IDR 4000.000 - IDR 5000.000</p>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="loker-detail-list">
                                            <p class="h5 font-weight-bold"><i class="fas fa-align-left mr-4"></i>Detail Lowongan</p>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-3">
                                                <p>Perusahaan</p>
                                            </div>
                                            <div class="col-1">
                                                <p>:</p>
                                            </div>
                                            <div class="col-4">
                                                <p>{{$loker->perusahaan->nama_pt}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <p>Bidang</p>
                                            </div>
                                            <div class="col-1">
                                                <p>:</p>
                                            </div>
                                            <div class="col-4 pt-2">
                                                <p class="male-btn">{{$loker->bidangs->nama_bidang}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <p>Jenis Kelamin</p>
                                            </div>
                                            <div class="col-1">
                                                <p>:</p>
                                            </div>
                                            <div class="col-4">
                                                <p style="color: #9e32a9">{{$loker->jenis_kelamin}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <p>Kuota</p>
                                            </div>
                                            <div class="col-1">
                                                <p>:</p>
                                            </div>
                                            <div class="col-4">
                                                <p >{{$loker->kuota}} Pelamar</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <p>Status</p>
                                            </div>
                                            <div class="col-1">
                                                <p>:</p>
                                            </div>
                                            <div class="col-4 pt-2">
                                                <p class="male-btn" style="background-color: #17d27c; color:#fff">{{$loker->status}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <p>Daftar Sebelum</p>
                                            </div>
                                            <div class="col-1">
                                                <p>:</p>
                                            </div>
                                            <div class="col-4">
                                                @php
                                                    $batas = Carbon\Carbon::parse($loker->batas_akhir)->toDatetimeString();
                                                @endphp
                                                <p>{{ $loker->batas_akhir}}</p>
                                            </div>
                                        </div>
                                        <div class="loker-detail-list mt-4">
                                            <p class="h5 font-weight-bold"><i class="fas fa-sticky-note mr-4"></i>Deskripsi Lowongan</p>
                                        </div>
                                        <div class="loker-detail-description">
                                            <?= $loker->deskripsi ?>
                                        </div>
                                        <div class="loker-detail-list mt-4">
                                            <p class="h5 font-weight-bold"><i class="fas fa-list mr-4"></i>Syarat & Ketentuan Lowongan</p>
                                        </div>
                                        <div class="loker-detail-terms">
                                            <?= $loker->sk ?>
                                        </div>
                                         <div class="loker-detail-list mt-4">
                                            <p class="h5 font-weight-bold"><i class="fas fa-file-pdf mr-4"></i>Berkas Persyaratan</p>
                                            <a class="btn head-btn1 ml-5 mt-3" style="padding: 15px 15px" href="{{route('persyaratan.download', $loker->id)}}" style="padding: 20px 20px"><i class="fas fa-download mr-2"></i>Download</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="card shadow-sm">
                                    @php
                                        $pd = App\Model\Pendaftar::where('loker_id', $loker->id)->where('user_id', Auth::id())->first();
                                    @endphp
                                    <div class="card-body text-center @if($pd) pt-4 pb-4 @endif">
                                        @if(!$pd)
                                            <a class="btn head-btn1 " data-toggle="collapse" href="#form-pendaftaran" role="button" aria-expanded="false" aria-controls="collapseExample" style="padding: 20px 20px"><i class="fas fa-paper-plane mr-2"></i> Daftar Sekarang</a>
                                        @else
                                            <button class="bg-secondary mx-3 text-white"  style="padding: 20px 20px;" disabled><i class="fas fa-paper-plane mr-2"></i> Daftar Sekarang</button>
                                        @endif
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                <a class="btn head-btn2" href="#" onclick="openTimeline()" style="padding: 20px 20px"><i class="fas fa-calendar-alt mr-2"></i>Buka Timeline</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3" >
                            <div class="col-12">
                                <div class="collapse" id="form-pendaftaran">
                                    <div class="card shadow-sm">
                                        <div class="card-body">
                                            <div class="form-group text-center">
                                                <label for="" class="text-center font-weight-bold" style="color: #9e32a9">Pendaftaran</label>
                                            </div>
                                            <form action="{{route('user.pendaftaran.save')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="loker_id" value="{{ $loker->id}}">
                                                <div class="form-group">
                                                    <label for="" class="font-weight-bold">NIK</label>
                                                    <input type="text" name="nik" class="form-control @error('nik') is-invalid @enderror" placeholder="Input NIK..">
                                                    @error('nik')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="font-weight-bold">Nomor Telepon Aktif</label>
                                                    <input type="text" name="no_telp" class="form-control @error('no_telp') is-invalid @enderror"  placeholder="Input nomor telepon..">
                                                    @error('no_telp')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="font-weight-bold">Agama</label>
                                                    <input type="text" name="agama" class="form-control @error('agama') is-invalid @enderror" placeholder="Input agama..">
                                                    @error('agama')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" value="Daftar" class="btn head-btn1" style="padding: 17px 17px">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <div class="card shadow-sm">
                                    <div class="card-body">
                                        <div class="loker-detail-list mt-2">
                                            <p class="h5 font-weight-bold"><i class="fas fa-building mr-4"></i>Profil Perusahaan</p>
                                        </div>
                                        <div class="col-lg-12 col-12 text-center">
                                            <img src="{{url('foto-PT/'. $loker->perusahaan->photo)}}" class="company_image_loker2 img-thumbnail" alt="">
                                            <p class="h5 font-weight-bold mt-2" style="color: #9e32a9">{{$loker->perusahaan->nama_pt}}</p>
                                        </div>
                                        <div class="col-12">
                                            {!! $loker->perusahaan->deskripsi_excerpt !!} <span><a href="#" style="text-decoration: none; color:#9e32a9;">Read More</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <div class="card shadow-sm">
                                    <div class="card-body">
                                        <div class="loker-detail-list mt-2">
                                            <p class="h5 font-weight-bold"><i class="fas fa-search-location mt-4"></i>Lokasi Perusahaan</p>
                                        </div>
                                        <div style="width: 100%"><iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?= $loker->perusahaan->map ?>"></iframe><a href="https://www.maps.ie/route-planner.htm">Road Trip Planner</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Job List Area End -->

        <div class="modal fade" id="exampleModalCenter" style="border-radius:20px" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="mr-2 mt-2">
                    <a href="#" onclick="closeModal()" class="close close-icon float-right"></a>
                </div>
                <div class="upload-title ml-3 mt-3">
                  <h5 class="modal-title" id="exampleModalLongTitle">Upload Berkas Pribadi & Persyaratan</h5>
                </div>
                <div class="modal-body">
                    <div class="row mt-3">
                        <div class="col-6">
                            <p class="font-weight-bold">Berkas Pribadi</p>
                        </div>
                        <div class="col-6">
                            <a href="#" class="detail_loker-btn" onclick="uploadFile('dokumen_pribadi')">Upload</a>
                        </div>
                        <div class="col-12">
                            <div style="width:40%; border-bottom:1px solid #eceff8; " >
                                <p style="margin:0" class="text-secondary" id="dokumen_pribadi_text">ekstensi dalam .zip, .rar</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row mt-3">
                        <div class="col-6">
                            <p class="font-weight-bold">Berkas Persyaratan</p>
                        </div>
                        <div class="col-6">
                            <a href="#" class="detail_loker-btn" onclick="uploadFile('dokumen_persyaratan')">Upload</a>
                        </div>
                        <div class="col-12">
                            <div style="width:40%; border-bottom:1px solid #eceff8; " >
                                <p style="margin:0" class="text-secondary" id="dokumen_persyaratan_text">ekstensi dalam .zip, .rar</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="" method="post" enctype="multipart/form-data">
                        <input type="file" class="d-none" onchange="previewFile('dokumen_pribadi');" id="inp_dokumen_pribadi" name="dokumen_pribadi">
                        <input type="file" class="d-none" onchange="previewFile('dokumen_persyaratan');" id="inp_dokumen_persyaratan" name="dokumen_persyaratan">
                        <button type="button" class="btn head-btn2" style="padding: 20px 20px;" data-dismiss="modal">Close</button>
                        <button type="button" class="btn head-btn1"  style="padding: 20px 20px;">Save changes</button>
                    </form>
                </div>
              </div>
            </div>
          </div>
        <!--Pagination Start  -->
        
        <!--Pagination End  -->

    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')

    @if (count($errors) > 0)
    <script type="text/javascript">
        $( document ).ready(function() {
             $('#form-pendaftaran').collapse('show');
        });
    </script>
   @endif

    <script>
        function openTimeline(){
            event.preventDefault();
            if($("#timeline").hasClass("d-none")){
                $("#timeline").removeClass("d-none");
            }else{
                $("#timeline").addClass("d-none");
            }
        }
        function closeModal(){
            event.preventDefault();
            $('#exampleModalCenter').modal('hide');
        }

        function uploadFile(value){
                event.preventDefault();
                $("#inp_"+value).trigger('click');
                return false;
        }

        function previewFile(value){
            var file = $('#inp_'+value)[0].files[0].name;
            $('#'+value+"_text").text(file);
        }
    </script>
    </body>
</html>