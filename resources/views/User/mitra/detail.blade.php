<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        @include('user.layouts.head')
   </head>

   <body>
    <!-- Preloader Start -->
    {{-- <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/user/img/logo/logo1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Preloader Start -->
      @include('user.layouts.navbar')
    <main>

        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{url('banner-PT/'.$ps->gambar_banner)}}">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>{{$ps->nama_pt}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End -->
        <!-- Job List Area Start -->
        <div class="job-listing-area pt-80 pb-120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow-sm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-2 col-md-6 col-12">
                                        <img src="{{url('foto-PT/'. $ps->photo)}}" class="company_image img-thumbnail" alt="">
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="h4">
                                            {{$ps->nama_pt}}
                                        </div>
                                        <div class="h5 text-secondary">
                                            {{$ps->bidangs->nama_bidang}}
                                        </div>
                                        <div>
                                            <p><i class="far fa-calendar-check mr-2"></i>Member sejak {{$ps->created_at->isoFormat('D MMMM Y')}} </p>
                                        </div>
                                        <div>
                                            <p><i class="fas fa-map-marker-alt mr-2"></i>{!!$ps->alamat!!} </p>
                                        </div>
                                    </div>
                                </div>
                                <hr class="text-secondary">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6 col-lg-2">
                                            <a href="#" class="btn head-btn2" style="padding: 20px 20px">Riwayat Timeline</a>
                                        </div>
                                        <div class="col-6 col-lg-2">
                                            <a href="#" class="btn head-btn1" style="padding: 20px 20px">Tambah Favorit</a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card shadow-sm">
                                    <div class="card-body">
                                        <div class="company-profile-title">
                                            <p class="h3 font-weight-bold" style="color: #9e32a9">Tentang Perusahaan</p>
                                        </div>
                                        <div class="company-profile-desc">
                                            {!! $ps->deskripsi !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-12">
                                <div class="card shadow-sm">
                                    <div class="card-body">
                                        <div class="job-seeker">
                                            <p class="h3 font-weight-bold" style="color: #9e32a9">Lowongan Kerja yang Terbuka</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            @foreach ($loker as $loker)
                                <div class="col-lg-12">
                                    <div class="card shadow-sm">
                                        <div class="card-body">
                                            <div class="row my-3">
                                                <div class="col-lg-2 col-md-6 col-12">
                                                    <img src="{{url('foto-PT/'. $ps->photo)}}" class="company_image_loker img-thumbnail" alt="">
                                                </div>
                                                <div class="col-lg-5 col-md-6 col-12">
                                                    <div class="loker_title h5 font-weight-bold">
                                                        {{$loker->nama_loker}}
                                                    </div>
                                                    <div class="loker_bidang">
                                                        {{$loker->bidangs->nama_bidang}}
                                                    </div>
                                                    <div class="loker_thum d-flex" >
                                                        <a class="male-btn mr-2" style="text-decoration: none;color:#fff">{{$loker->jenis_kelamin}}</a>
                                                        <span style="display: table-cell;vertical-align: middle;" >  - kuota {{$loker->kuota}} Orang</span> 
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-12 text-center">
                                                    <a href="{{route('lowongan.detail', $loker->id)}}" class="detail_loker-btn">Detail</a>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-lg-12" style="padding: 0">
                                                    {!! $loker->deskripsi_excerpt !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="card shadow-sm">
                            <div class="card-body">
                                <div class="company-profile-title">
                                    <p class="h3 font-weight-bold" style="color: #9e32a9">Lokasi Perusahaan</p>
                                </div>
                                <div style="width: 100%"><iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?= $ps->map ?>"></iframe><a href="https://www.maps.ie/route-planner.htm">Road Trip Planner</a></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Job List Area End -->
        <!--Pagination Start  -->
        
        <!--Pagination End  -->

    </main>
    <!-- Footer Start-->
    @include('user.layouts.footer')

  <!-- JS here -->
	@include('user.layouts.js')
		
        
    </body>
</html>