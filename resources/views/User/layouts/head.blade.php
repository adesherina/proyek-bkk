<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>BKK SMK PGRI JATIBARANG</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon" href="{{url('assets/user/img/20215971.png')}}">

<!-- CSS here -->
<link rel="stylesheet" href="{{url('assets/user/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/flaticon.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/price_rangs.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/slicknav.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/animate.min.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/fontawesome-all.min.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/slick.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/nice-select.css')}}">
<link rel="stylesheet" href="{{url('assets/user/css/style.css')}}">