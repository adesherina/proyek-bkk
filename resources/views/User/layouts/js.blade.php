<!-- All JS Custom Plugins Link Here here -->
        <script src="{{url('assets/user/js/vendor/modernizr-3.5.0.min.js')}}"></script>
		<!-- Jquery, Popper, Bootstrap -->
		<script src="{{url('assets/user/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{url('assets/user/js/popper.min.js')}}"></script>
        <script src="{{url('assets/user/js/bootstrap.min.js')}}"></script>
	    <!-- Jquery Mobile Menu -->
        <script src="{{url('assets/user/js/jquery.slicknav.min.js')}}"></script>

		<!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="{{url('assets/user/js/owl.carousel.min.js')}}"></script>
        <script src="{{url('assets/user/js/slick.min.js')}}"></script>
        <script src="{{url('assets/user/js/price_rangs.js')}}"></script>
        
		<!-- One Page, Animated-HeadLin -->
        <script src="{{url('assets/user/js/wow.min.js')}}"></script>
		<script src="{{url('assets/user/js/animated.headline.js')}}"></script>
        <script src="{{url('assets/user/js/jquery.magnific-popup.js')}}"></script>

		<!-- Scrollup, nice-select, sticky -->
        <script src="{{url('assets/user/js/jquery.scrollUp.min.js')}}"></script>
        <script src="{{url('assets/user/js/jquery.nice-select.min.js')}}"></script>
		<script src="{{url('assets/user/js/jquery.sticky.js')}}"></script>
        
        <!-- contact js -->
        <script src="{{url('assets/user/js/contact.js')}}"></script>
        <script src="{{url('assets/user/js/jquery.form.js')}}"></script>
        <script src="{{url('assets/user/js/jquery.validate.min.js')}}"></script>
        <script src="{{url('assets/user/js/mail-script.js')}}"></script>
        <script src="{{url('assets/user/js/jquery.ajaxchimp.min.js')}}"></script>
        
		<!-- Jquery Plugins, main Jquery -->	
        <script src="{{url('assets/user/js/plugins.js')}}"></script>
        <script src="{{url('assets/user/js/main.js')}}"></script>