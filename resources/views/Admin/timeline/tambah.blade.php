@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Timeline</h1>
      
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Timeline</a></div>
        <div class="breadcrumb-item">Timeline</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('timeline.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Timeline</h4>
                <div class="card-header-action">
                    <a href="{{url('timeline/list')}}" class="btn btn-success btn-lg">
                      kembali
                    </a>
                  </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                    <label >Nama Loker</label>
                    @php
                    $loker = DB::table('loker')->get();    
                    @endphp
                        <select class="form-control selectric" name="loker_id">
                          @foreach ($loker as $item)
                            <option value="{{$item->id}}">{{$item->nama_loker}}</option>
                          @endforeach
                        </select>
                        @if ($errors->has('loker_id'))
                          <span class="text-danger">{{ $errors->first('loker_id') }}</span>
                        @endif
                </div>
                <div class="form-group">
                  <label>Tanggal Pembukaan</label>
                  <input type="date" class="form-control" name="tanggal_pembukaan">
                  @if ($errors->has('tanggal_pembukaan'))
                  <span class="text-danger">{{ $errors->first('tanggal_pembukaan') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Tanggal Penutupan</label>
                  <input type="date" class="form-control" name="tanggal_penutupan">
                  @if ($errors->has('tanggal_penutupan'))
                  <span class="text-danger">{{ $errors->first('tanggal_penutupan') }}</span>
                  @endif
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi" id="editor2"></textarea>
                 @if ($errors->has('deskripsi'))
                  <span class="text-danger">{{ $errors->first('deskripsi') }}</span>
                  @endif
                </div>
                <div class="card-footer text-right">
                  <input type="submit" class="btn btn-primary mr-1" value="Submit">
                  <p class="h4 font-weight-bold">Data Tidak Ada</p>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'sk' );
      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 