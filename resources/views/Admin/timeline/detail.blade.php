@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Timeline</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Timeline</a></div>
          <div class="breadcrumb-item">Timeline</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail </h4>
            <div class="card-header-action ">
              <a href="{{url('timeline/list')}}" class="btn btn-success btn-lg float-right">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                          @php
                          $loker = DB::table('loker')->first();    
                          @endphp
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Loker</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary">{{$loker->nama_loker}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Tanggal Pembukaan</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$timeline->tanggal_pembukaan}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Tanggal Penutupan</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$timeline->tanggal_penutupan}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Deskripsi</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                <?= $timeline->deskripsi ?>
                              </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection