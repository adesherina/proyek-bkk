@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Timeline</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Timeline</a></div>
                <div class="breadcrumb-item">Data Timeline</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @elseif(session('status2'))
                <div class="alert alert-danger">
                    {{ session('status2') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Timeline</h4>
                <div class="card-header-action">
                  <a href="{{route('timeline.tambahdata')}}" class="btn btn-success btn-lg">
                    Add
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Loker</th>
                        <th>Nama Perusahaan</th>
                        <th>Tanggal Pembukaan</th>
                        <th>Tanggal Penutupan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($timeline as $timeline)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$timeline->lokerRef->nama_loker}}</td>
                        <td>{{$timeline->lokerRef->perusahaan->nama_pt}}</td>
                        <td>{{$timeline->tanggal_pembukaan}}</td>
                        <td>{{$timeline->tanggal_penutupan}}</td>
                        <td>
                          <a href="{{route('timeline.editdata', $timeline->id)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                          </a>
                          <form action="{{route('timeline.deletedata', $timeline->id)}}" id="delete{{$timeline->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                          @method('delete')
                          <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                          @csrf
                          </form>
                          <a href="{{route('timeline.detaildata', $timeline->id)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                        </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 