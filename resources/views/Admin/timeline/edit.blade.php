@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Timeline</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Timeline</a></div>
        <div class="breadcrumb-item">Timeline</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('timeline.update', $timeline->id)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Timeline</h4>
                <div class="card-header-action">
                  <a href="{{url('timeline/list')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                    <label >Nama Loker</label>
                    @php
                    $loker = DB::table('loker')->get();    
                    @endphp
                        <select class="form-control selectric" name="loker_id">
                          <option value="{{$timeline->lokerRef->id}}" selected>{{$timeline->lokerRef->nama_loker}}</option>
                          @foreach ($loker as $item)
                            
                            <option value="{{$item->id}}">{{$item->nama_loker}}</option>
                          @endforeach
                        </select>
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi">{{$timeline->deskripsi}}</textarea>
                </div>
                <div class="form-group">
                  <label>Tanggal Pembukaan</label>
                  <input type="date" class="form-control" name="tanggal_pembukaan" value="{{$timeline->tanggal_pembukaan}}">
                </div>
                <div class="form-group">
                  <label>Tanggal Penutupan</label>
                  <input type="date" class="form-control" name="tanggal_penutupan" value="{{$timeline->tanggal_penutupan}}">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 
@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'sk' );
      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush