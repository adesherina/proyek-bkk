@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Pendaftar Pekerjaan</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Pendaftar Pekerjaan</a></div>
                <div class="breadcrumb-item">Data Pendaftar Pekerjaan</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @elseif(session('status2'))
                <div class="alert alert-danger">
                    {{ session('status2') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Pendaftar Pekerjaan</h4>
                {{-- <div class="card-header-action">
                  <a href="" class="btn btn-success btn-lg">
                    Verified
                  </a>
                  <a href="" class="btn btn-warning btn-lg">
                    Unverified
                  </a>
                  <a href="" class="btn btn-danger btn-lg">
                    Reject
                  </a>
                </div> --}}
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>Nomor Telepon</th>
                        <th>Nama Perusahaan</th>
                        <th>Nama Loker</th>
                        <th>Status</th>
                        <th>Registrasi</th>
                        <th>Tanggal Daftar</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($pd as $item)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$item->userRef->nama_lengkap}}</td>
                        <td>{{$item->no_telp}}</td>
                        <td>{{$item->lokerRef->perusahaan->nama_pt}}</td>
                        <td>{{$item->lokerRef->nama_loker}}</td>
                        <td>{{$item->status}}</td>
                        <td>{{$item->registrasi}}</td>
                        <td>{{date('d-m-Y', strtotime($item->created_at))}}</td>
                        <td>
                          <div class="d-flex">
                              <a href="{{route('pendaftaran.detaildata', $item->id)}}" class="btn btn-icon icon-left btn-primary mr-3"><i class="fas fa-eye"></i></a>
                              @if ($item->registrasi === "pending")
                                <a href="{{route('pendaftaran.lunas', $item->id)}}" onclick="makeLunas('{{$item->id}}')" title="Buat lunas" class="btn btn-icon icon-left btn-success"><i class="fas fa-money-bill-wave-alt"></i></a>  
                              @else
                                <a href="{{route('pendaftaran.pending', $item->id)}}" onclick="makePending('{{$item->id}}')" title="Buat pending" class="btn btn-icon icon-left btn-success"><i class="fas fa-check"></i></a>  
                              @endif
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection

@push('script')

    <script>
          function makeLunas(id) {
                event.preventDefault();
                console.log(id);
                swal({
                    title: 'Apakah Anda Yakin?',
                    showCancelButton: true,
                    confirmButtonColor: '#7fad39',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, buat lunas!'
                })
                .then(function (success) {
                    if (success) {
                      console.log('hiyaa');
                        var url = '{{ url("pendaftaran/lunas/") }}/'+id;
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "GET",
                            success: function (data) {
                                console.log(data);
                                if (data.status == 1) {
                                    swal({
                                        title: "Success!",
                                        type: "success",
                                        text: "Pendafataran telah lunas \n Click OK",
                                        icon: "success",
                                        confirmButtonClass: "btn btn-outline-info",
                                    });
                                    location.reload();
                                }
                            },
                            error: function (error) {
                                console.log(error);
                                swal({
                                    title: 'Opps...',
                                    text: error.message,
                                    type: 'error',
                                    timer: '1500'
                                })
                            }
                        });
                    }
                });
            };

            function makePending(id) {
                event.preventDefault();
                console.log(id);
                swal({
                    title: 'Apakah Anda Yakin?',
                    showCancelButton: true,
                    confirmButtonColor: '#7fad39',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, buat pending!'
                })
                .then(function (success) {
                    if (success) {
                      console.log('hiyaa');
                        var url = '{{ url("pendaftaran/pending/") }}/'+id;
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "GET",
                            success: function (data) {
                                console.log(data);
                                if (data.status == 1) {
                                    swal({
                                        title: "Success!",
                                        type: "success",
                                        text: "Pembayaran pendafataran pending \n Click OK",
                                        icon: "success",
                                        confirmButtonClass: "btn btn-outline-info",
                                    });
                                    location.reload();
                                }
                            },
                            error: function (error) {
                                console.log(error);
                                swal({
                                    title: 'Opps...',
                                    text: error.message,
                                    type: 'error',
                                    timer: '1500'
                                })
                            }
                        });
                    }
                });
            };
    </script>
@endpush
 