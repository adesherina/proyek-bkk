@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Pendaftaran Pekerjaan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Pendaftaran Pekerjaan</a></div>
          <div class="breadcrumb-item">Pendaftaran Pekerjaan</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        @if (session('failed'))
        <div class="alert alert-danger">
          {{ session('failed') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail </h4>
            <div class="card-header-action ">
              <a href="{{url('pendaftaran/list')}}" class="btn btn-success btn-lg float-right">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">NIK</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$pendaftaran->nik}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Lengkap</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$pendaftaran->userRef->nama_lengkap}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nomor Telepon</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$pendaftaran->no_telp}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Agama</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$pendaftaran->agama}}</p>
                            </div> 
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Perusahaan</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$pendaftaran->lokerRef->perusahaan->nama_pt}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Tanggal Daftar</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary">{{date('d-m-Y', strtotime($pendaftaran->created_at))}}</p>
                            </div>
                        </div>
                        @php
                            $bk = App\Model\Berkas::where('pendaftaran_id', $pendaftaran->id)->first();
                        @endphp
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Berkas Pribadi</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8 d-flex">
                               <p class="card-text text-primary">@if($bk) {{$bk->berkas_pribadi}} @endif</p>
                               @if ($bk)
                                  <p><a href="{{route('berkas.pribadi.download', $pendaftaran->id)}}" class="btn btn-success btn-lg ml-4" style="border-radius:20px">Download</a> </p>
                               @endif
                               
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Berkas Persyaratan</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8 d-flex">
                               <p class="card-text text-primary">@if($bk) {{$bk->berkas_persyaratan}} @endif</p>
                               @if ($bk)
                                  <p> <a href="{{route('berkas.persyaratan.download', $pendaftaran->id)}}" class="btn btn-primary btn-lg ml-4" style="border-radius:20px">Download</a> </p> 
                               @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                          <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                              <p class="card-text text-primary">Status</p>
                          </div>
                          <div class="col-1">
                            :
                          </div>
                          <div class="col-8">
                             <select name="status" id="status" class="form-control">
                                @if ($pendaftaran->status === "unverified")
                                  <option selected value="{{$pendaftaran->status}}">{{$pendaftaran->status}}</option>
                                  <option selected value="verified">Verified</option>
                                  <option selected value="reject">reject</option>
                                @elseif($pendaftaran->status === "verified")
                                  <option selected value="{{$pendaftaran->status}}">{{$pendaftaran->status}}</option>
                                  <option selected value="unverified">Unverified</option>
                                  <option selected value="reject">reject</option>
                                @else
                                  <option selected value="{{$pendaftaran->status}}">{{$pendaftaran->status}}</option>
                                  <option selected value="verified">verified</option>
                                  <option selected value="unverified">Unverified</option>
                                @endif
                             </select>
                          </div>
                      </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

      $('#status').on('change', function(){
        var status = $(this).val();

        swal({
                    title: 'Apakah Anda Yakin?',
                    showCancelButton: true,
                    confirmButtonColor: '#7fad39',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, '+status+ ' !'
                })
                .then(function (success) {
                    if (success) {
                      console.log('hiyaa');
                        var url = '{{ route("pendaftaran.change.status") }}';
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "POST",
                            data:{
                              status:status,
                              id:"{{$pendaftaran->id}}"
                            },
                            success: function (data) {
                                console.log(data);
                                if (data.status == 1) {
                                    swal({
                                        title: "Success!",
                                        type: "success",
                                        text: "Pendafataran telah di"+status+" \n Click OK",
                                        icon: "success",
                                        confirmButtonClass: "btn btn-outline-info",
                                    });
                                    location.reload();
                                }
                            },
                            error: function (error) {
                                console.log(error);
                                swal({
                                    title: 'Opps...',
                                    text: error.message,
                                    type: 'error',
                                    timer: '1500'
                                })
                            }
                        });
                    }
                });
      })
    </script>
@endpush