<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Admin</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">Admin</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Dashboard</li>
              <li class="nav-item ">
                <a href="{{url('/dashboard')}}" class="nav-link "><i class="fas fa-fire"></i><span>Dashboard</span></a>
              </li>
              <li class="menu-header">Pengguna</li>
              <li class="nav-item ">
                <a href="#" class="nav-link has-dropdown" ><i class="fas fa-user"></i><span>User</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{url('dataadmin')}}">Admin</a></li>
                  <li><a class="nav-link" href="{{url('datauser')}}">Pengguna</a></li>
                </ul>
              </li>
              <li class="menu-header">Perusahaan</li>
              <li class="nav-item ">
                <a href="{{route('bidang.index')}}" class="nav-link "><i class="fas fa-building"></i><span>Bidang Perusahaan</span></a>
              </li>
              <li class="nav-item ">
                <a href="{{url('pt/list')}}" class="nav-link "><i class="fas fa-industry"></i><span>Perusahaan</span></a>
              </li>
              <li class="menu-header">Lowongan Kerja</li>
              <li class="nav-item ">
                <a href="{{url('loker/list')}}" class="nav-link "><i class="fas fa-notes-medical"></i><span>Lowongan</span></a>
              </li>
              <li class="nav-item ">
                <a href="{{url('pendaftaran/list')}}" class="nav-link "><i class="fas fa-users"></i><span>Pendaftar Kerja</span></a>
              </li>
              <li class="nav-item ">
                <a href="{{url('timeline/list')}}" class="nav-link "><i class="fas fa-calendar-alt"></i><span>Timeline</span></a>
              </li>
              <li class="menu-header">Setting</li>
              <li class="nav-item ">
                <a href="{{url('email/list')}}" class="nav-link "><i class="fa fa-cog" aria-hidden="true"></i><span>Email</span></a>
              </li>
              
        </aside>
      </div>