<nav class="navbar navbar-expand-lg main-navbar">
    <div class="container">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            @php
                use Illuminate\Support\Facades\DB;
                use Illuminate\Support\Facades\Session;
                $you = DB::table('admin')->where('id', Session::get('id'))->first();
                
            @endphp
            <img alt="image" src="{{url('admin-profil/'. $you->photo)}}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{Session::get('name')}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="{{url('admin/profil/' .Session::get('id'))}}" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profil
              </a>
              <a href="{{url('admin/pw/' .Session::get('id'))}}" class="dropdown-item has-icon">
                <i class="fas fa-user-edit"></i>Ganti Password
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{url('logout')}}" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
    </div>
</nav>