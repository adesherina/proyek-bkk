@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail User</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail User</a></div>
          <div class="breadcrumb-item">User</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail User</h4>
            <div class="card-header-action">
              <a href="{{url('datauser')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Lengkap</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$user->nama_lengkap}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Email</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$user->email}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Alamat</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$user->alamat}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Tinggi Badan</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$user->tb}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Berat Badan</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary">{{$user->bb}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nilai</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary">{{$user->nilai}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Tahun Lulus</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary">{{$user->tahun_lulus}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Foto</p>
                            </div>
                            <div class="col-1">
                            :
                            </div>
                            <div class="col-8">
                            <img src="{{url('/foto-user/'.$user->photo)}}" style="width: 200px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection