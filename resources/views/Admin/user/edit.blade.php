@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Admin</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Admin</a></div>
        <div class="breadcrumb-item">Form Update Admin</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('user.update', $user->id)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Update Admin</h4>
                <div class="card-header-action">
                  <a href="{{url('datauser')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama_lengkap" value="{{$user->nama_lengkap}}">
                </div>
                <div class="form-group">
                  <label>E-mail</label>
                  <input type="text" class="form-control" name="email" value="{{$user->email}}">
                </div>
                <div class="form-group"> 
                  <label>Alamat</label>
                  <textarea class="form-control" name="alamat">{{$user->alamat}}</textarea>
                </div>
                <div class="form-group">
                    <label class="d-block">Jenis Kelamin</label>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios1" <?php if($user->jenis_kelamin == "Laki - Laki"){ ?> checked <?php } ?>  value="Laki - Laki">
                        <label class="form-check-label" for="exampleRadios1">
                          Laki - Laki 
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios2" <?php if($user->jenis_kelamin == "Perempuan"){ ?> checked <?php } ?>  value="Perempuan">
                        <label class="form-check-label" for="exampleRadios2">
                          Perempuan
                        </label>
                      </div>
                </div>
                <div class="form-group">
                  <label>Tinggi Badan</label>
                  <input type="text" class="form-control" name="tb" value="{{$user->tb}}">
                </div>
                <div class="form-group">
                  <label>Berat Badan</label>
                  <input type="text" class="form-control" name="bb" value="{{$user->bb}}">
                </div>
                <div class="form-group">
                  <label>Nilai </label>
                  <input type="text" class="form-control" name="nilai" value="{{$user->nilai}}">
                </div>
                
                <div class="form-group">
                  <label>Tahun Lulus</label>
                  <input type="date" class="form-control" name="tahun_lulus" value="{{$user->tahun_lulus}}">
                </div>
                <div class="form-group"> 
                    <label>Foto </label>
                   <input type="file" class="form-control" name="photo">
                   <input type="hidden" class="form-control" name="oldPhoto" value="{{$user->photo}}">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 