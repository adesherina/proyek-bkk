@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>User</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data User</a></div>
                <div class="breadcrumb-item">User</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data User</h4>
                <div class="card-header-action">
                  <a href="#" class="btn btn-primary">
                    View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive" >
                  <table id="tabel-data" class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>E-Mail</th>
                        <th>Jenis Kelamin</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($users as $user)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$user->nama_lengkap}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->jenis_kelamin}}</td>
                        <td>
                          <a href="{{route('user.editdata', $user->id)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                          </a>
                          <form action="{{url('datauser/delete', $user->id)}}" id="delete{{$user->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                              @method('delete')
                              <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                              @csrf
                          </form>
                          <a href="{{route('user.detaildata', $user->id)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                        </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 