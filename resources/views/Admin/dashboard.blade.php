@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>
    
    <div class="section-body">
      <div class="container-fluid"> 
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <i class="far fa-user"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  use Illuminate\Support\Facades\DB;
                  $admin = DB::table('admin')->get();
                  $count = $admin->count();
                ?>
                  <div class="card-header">
                    <h4>Total admin</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>  

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-info">
                <i class="fas fa-user-friends"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $users = DB::table('users')->get();
                  $count = $users->count();
                ?>
                  <div class="card-header">
                    <h4>Total Pengguna</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-primary">
                <i class="fas fa-user-friends"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $pt = DB::table('pt')->get();
                  $count = $pt->count();
                ?>
                  <div class="card-header">
                    <h4>Total Perusahaan</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-primary">
                <i class="fas fa-notes-medical"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $loker = DB::table('loker')->get();
                  $count = $loker->count();
                ?>
                  <div class="card-header">
                    <h4>Total Lowongan Pekerjaan</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <i class="fas fa-users"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $pndf = DB::table('pendaftaran')->get();
                  $count = $pndf->count();
                ?>
                  <div class="card-header">
                    <h4>Total Pendaftar Pekerjaan</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-info">
                <i class="fas fa-calendar-alt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $timeline = DB::table('timeline')->get();
                  $count = $timeline->count();
                ?>
                  <div class="card-header">
                    <h4>Total Timeline</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

        </div> 
      </div>
    </div>
  </section>
</div> 
@endsection
 