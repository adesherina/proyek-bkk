@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Perusahaan</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Perusahaan</a></div>
                <div class="breadcrumb-item">Data Perusahaan</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @elseif(session('status2'))
                <div class="alert alert-danger">
                    {{ session('status2') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Perusahaan</h4>
                <div class="card-header-action">
                  <a href="{{route('pt.tambahdata')}}" class="btn btn-success btn-lg">
                    Add
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Perusahaan</th>
                        <th>Bidang Perusahaan</th>
                        <th>Nomor Telepon</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($pt as $pt)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$pt->nama_pt}}</td>
                        <td>
                          @if (isset($pt->bidangs))
                              {{$pt->bidangs->nama_bidang}}
                          @endif
                        </td>
                        <td>{{$pt->no_telp}}</td>
                        <td>{{$pt->email}}</td>
                        <td>
                          <a href="{{route('pt.editdata', $pt->id)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                          </a>
                          <form action="{{route('pt.deletedata', $pt->id)}}" id="delete{{$pt->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                          @method('delete')
                          <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                          @csrf
                          </form>
                          <a href="{{route('pt.detaildata', $pt->id)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                        </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 