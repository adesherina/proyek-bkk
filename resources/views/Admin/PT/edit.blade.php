@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Admin</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Admin</a></div>
        <div class="breadcrumb-item">Form Update Admin</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('pt.update', $pt->id)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Update Admin</h4>
                <div class="card-header-action">
                  <a href="{{url('pt/list')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Perusahaan</label>
                  <input type="text" class="form-control" name="nama_pt" value="{{$pt->nama_pt}}">
                </div>
                <div class="form-group">
                    <label >Bidang</label>
                    @php
                    $bidang = DB::table('bidang')->get();    
                    @endphp
                        <select class="form-control selectric" name="bidang">
                          <option value="{{$pt->bidangs->id}}" selected>{{$pt->bidangs->nama_bidang}}</option>
                          @foreach ($bidang as $item)
                            <option value="{{$item->id}}">{{$item->nama_bidang}}</option>
                          @endforeach
                        </select>
                </div>
                <div class="form-group">
                  <label>Nomor Telepon</label>
                  <input type="text" class="form-control" name="no_telp" value="{{$pt->no_telp}}">
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" name="email" value="{{$pt->email}}">
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi">{{$pt->deskripsi}}</textarea>
                </div>
                <div class="form-group"> 
                  <label>Alamat</label>
                 <textarea class="form-control" style="height: 200px" name="alamat">{{$pt->alamat}}</textarea>
                </div>
                <div class="form-group"> 
                    <label>Foto Perusahaan</label>
                   <input type="file" class="form-control" name="photo">
                   <input type="hidden" class="form-control" name="oldPhoto" value="{{$pt->photo}}">
                </div>
                <div class="form-group"> 
                    <label>Banner Perusahaan (200px x 400px)</label>
                   <input type="file" class="form-control" name="banner">
                   <input type="hidden" class="form-control" name="oldBanner" value="{{$pt->banner}}">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
     $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 