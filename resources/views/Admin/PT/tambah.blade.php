@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Perusahaan</h1>
      
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Perusahaan</a></div>
        <div class="breadcrumb-item">Tambah Perusahaan</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('pt.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Perusahaan</h4>
                <div class="card-header-action">
                    <a href="{{url('pt/list')}}" class="btn btn-success btn-lg">
                      kembali
                    </a>
                  </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Perusahaan</label>
                  <input type="text" class="form-control" name="nama_pt" required>
                </div>
                <div class="form-group">
                  <label>Bidang Perusahaan</label>
                  <select name="bidang" id="bidang"\>
                    <option selected>Pilih Bidang Perusahaan</option>
                    @foreach ($bidang as $item)
                      <option value="{{$item->id}}">{{$item->nama_bidang}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label>Nomor Telepon</label>
                  <input type="text" class="form-control" name="no_telp" required>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" name="email" required>
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                  <textarea class="form-control" name="deskripsi" id="editor1"></textarea>
                </div>
                <div class="form-group"> 
                  <label>Alamat</label>
                 <textarea class="form-control" style="height:200px" name="alamat"  id="editor2"></textarea>
                </div>
                <div class="form-group"> 
                  <label>Map</label>
                  <textarea class="form-control" name="map"></textarea>
                </div>
                <div class="form-group"> 
                    <label>Foto Perusahaan</label>
                   <input type="file" class="form-control" name="photo">
                </div>
                <div class="form-group"> 
                  <label>Banner Perusahaan (200px x 400px)</label>
                 <input type="file" class="form-control" name="banner">
              </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
     $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 