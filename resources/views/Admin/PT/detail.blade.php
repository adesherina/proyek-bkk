@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Perusahaan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Perusahaan</a></div>
          <div class="breadcrumb-item">Perusahaan</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Perusahaan</h4>
            <div class="card-header-action">
              <a href="{{url('pt/list')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Perusahaan</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$pt->nama_pt}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Alamat</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                <?= $pt->alamat ?>
                              </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Deskripsi</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                <?= $pt->deskripsi ?>
                              </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Foto Perusahaan</p>
                            </div>
                            <div class="col-1">
                            :
                            </div>
                            <div class="col-8">
                            <img src="{{url('/foto-PT/'.$pt->photo)}}" style="width: 200px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection