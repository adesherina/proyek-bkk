@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Lowongan Pekerjaan</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Lowongan Pekerjaan</a></div>
                <div class="breadcrumb-item">Data Lowongan Pekerjaan</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @elseif(session('status2'))
                <div class="alert alert-danger">
                    {{ session('status2') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Lowongan Pekerjaan</h4>
                <div class="card-header-action">
                  <a href="{{route('loker.tambahdata')}}" class="btn btn-success btn-lg">
                    Add
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Loker</th>
                        <th>Nama Perusahaan</th>
                        <th>Bidang</th>
                        <th>Kuota</th>
                        <th>Batas Akhir</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($loker as $loker)
                      <tr>
                        <td>{{$loop->iteration}}</td> 
                        <td>{{$loker->nama_loker}}</td> 
                        <td>{{$loker->perusahaan->nama_pt}}</td>
                        <td>
                          @if (isset($loker->bidangs))
                              {{$loker->bidangs->nama_bidang}}
                          @endif
                        </td>
                        <td>{{$loker->kuota}}</td>
                        <td>{{$loker->batas_akhir}}</td>
                        <td>
                          <a href="{{route('loker.editdata', $loker->id)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                          </a>
                          <form action="{{route('loker.deletedata', $loker->id)}}" id="delete{{$loker->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                          @method('delete')
                          <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                          @csrf
                          </form>
                          <a href="{{route('loker.detaildata', $loker->id)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                        </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 