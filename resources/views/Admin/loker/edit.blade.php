@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Lowongan Pekerjaan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Lowongan Pekerjaan</a></div>
        <div class="breadcrumb-item">Form Update Lowongan Pekerjaan</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('loker.update', $loker->id)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Update Lowongan Pekerjaan</h4>
                <div class="card-header-action">
                  <a href="{{url('loker/list')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Loker</label>
                  <input type="text" class="form-control" name="nama_loker" value="{{$loker->nama_loker}}">
                </div>
                <div class="form-group">
                    <label >Nama Perusahaan</label>
                    @php
                    $pt = DB::table('pt')->get();    
                    @endphp
                        <select class="form-control selectric" name="pt_id">
                          <option value="{{$loker->perusahaan->id}}" selected>{{$loker->perusahaan->nama_pt}}</option>
                          @foreach ($pt as $item)
                            <option value="{{$item->id}}">{{$item->nama_pt}}</option>
                          @endforeach
                        </select>
                </div>
                <div class="form-group">
                    <label >Bidang</label>
                    @php
                    $bidang = DB::table('bidang')->get();    
                    @endphp
                        <select class="form-control selectric" name="bidang">
                          <option value="{{$loker->bidangs->id}}" selected>{{$loker->bidangs->nama_bidang}}</option>
                          @foreach ($bidang as $item)
                            <option value="{{$item->id}}">{{$item->nama_bidang}}</option>
                          @endforeach
                        </select>
                </div>
                <div class="form-group">
                  <label class="d-block">Jenis Kelamin</label>
                    <select class="form-control selectric" name="jenis_kelamin">
                      <option value="{{$loker->jenis_kelamin}}" selected>{{$loker->jenis_kelamin}}</option>
                      <option value="Laki-laki">Laki-laki</option>
                      <option value="Perempuan">Perempuan</option>
                      <option value="Laki-laki & Perempuan">Laki-laki & Perempuan</option>
                    </select>
                </div>
                <div class="form-group"> 
                  <label>Syarat dan Ketentuan</label>
                 <textarea class="form-control" name="sk">{{$loker->sk}}</textarea>
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi">{{$loker->deskripsi}}</textarea>
                </div>
                <div class="form-group">
                  <label>Kuota</label>
                  <input type="text" class="form-control" name="kuota" value="{{$loker->kuota}}">
                </div>
                <div class="form-group">
                  <label>Batas Akhri</label>
                  <input type="date" class="form-control" name="batas_akhir" value="{{$loker->batas_akhir}}">
                </div>
                <div class="form-group">
                  <label>Formulir Pendaftaran</label>
                  <input type="file" class="form-control" name="formulir_pendaftaran">
                  <input type="hidden" class="form-control" name="oldFile" value="{{$loker->formulir_pendaftaran}}">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 
@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'sk' );
      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush