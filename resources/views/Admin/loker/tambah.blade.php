@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Lowongan Pekerjaan</h1>
      
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Lowongan Pekerjaan</a></div>
        <div class="breadcrumb-item">Tambah Lowongan Pekerjaan</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('loker.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Lowongan Pekerjaan<</h4>
                <div class="card-header-action">
                    <a href="{{url('loker/list')}}" class="btn btn-success btn-lg">
                      kembali
                    </a>
                  </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Loker</label>
                  <input type="text" class="form-control" name="nama_loker">
                  @if ($errors->has('nama_loker'))
                  <span class="text-danger">{{ $errors->first('nama_loker') }}</span>
                  @endif
                </div>
                <div class="form-group">
                    <label >Nama Perusahaan</label>
                    @php
                    $pt = DB::table('pt')->get();    
                    @endphp
                        <select class="form-control selectric" name="pt_id">
                          @foreach ($pt as $item)
                            <option value="{{$item->id}}">{{$item->nama_pt}}</option>
                          @endforeach
                        </select>
                        @if ($errors->has('pt_id'))
                          <span class="text-danger">{{ $errors->first('pt_id') }}</span>
                        @endif
                </div>
                <div class="form-group">
                    <label >Bidang</label>
                    @php
                    $bidang = DB::table('bidang')->get();    
                    @endphp
                        <select class="form-control selectric" name="bidang">
                          @foreach ($bidang as $item)
                            <option value="{{$item->id}}">{{$item->nama_bidang}}</option>
                          @endforeach
                        </select>
                        @if ($errors->has('bidang'))
                          <span class="text-danger">{{ $errors->first('bidang') }}</span>
                        @endif
                </div>
                <div class="form-group">
                  <label class="d-block">Jenis Kelamin</label>
                    <select class="form-control selectric" name="jenis_kelamin">
                      <option >Pilih Jenis Kelamin</option>
                      <option value="Laki-laki">Laki-laki</option>
                      <option value="Perempuan">Perempuan</option>
                      <option value="Laki-laki & Perempuan">Laki-laki & Perempuan</option>
                    </select>
                    @if ($errors->has('jenis_kelamin'))
                      <span class="text-danger">{{ $errors->first('jenis_kelamin') }}</span>
                    @endif
                </div>
                <div class="form-group"> 
                  <label>Syarat dan Ketentuan</label>
                 <textarea class="form-control" name="sk" id="editor1"></textarea>
                 @if ($errors->has('sk'))
                  <span class="text-danger">{{ $errors->first('sk') }}</span>
                  @endif
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi" id="editor2"></textarea>
                 @if ($errors->has('deskripsi'))
                  <span class="text-danger">{{ $errors->first('deskripsi') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Kuota</label>
                  <input type="text" class="form-control" name="kuota">
                  @if ($errors->has('kuota'))
                  <span class="text-danger">{{ $errors->first('kouta') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Range Gaji</label>
                  <input type="text" class="form-control" name="range_gaji">
                  @if ($errors->has('range_gaji'))
                  <span class="text-danger">{{ $errors->first('range_gaji') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Batas Akhri</label>
                  <input type="date" class="form-control" name="batas_akhir">
                  @if ($errors->has('batas_akhir'))
                  <span class="text-danger">{{ $errors->first('batas_akhir') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Formulir Pendaftaran</label>
                  <input type="file" class="form-control" name="formulir_pendaftaran">
                  @if ($errors->has('formulir_pendaftaran'))
                  <span class="text-danger">{{ $errors->first('formulir_pendaftaran') }}</span>
                  @endif
                </div>
                <div class="card-footer text-right">
                  <input type="submit" class="btn btn-primary mr-1" value="Submit">
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'sk' );
      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 