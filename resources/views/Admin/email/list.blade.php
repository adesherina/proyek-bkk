@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Data Email</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Email</a></div>
                <div class="breadcrumb-item">Data Email</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @elseif(session('status2'))
                <div class="alert alert-danger">
                    {{ session('status2') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Email</h4>
                <div class="card-header-action">
                  <a href="{{route('email.tambahdata')}}" class="btn btn-success btn-lg">
                    Add
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Driver</th>
                        <th>Host</th>
                        <th>Port</th>
                        <th>Username</th>
                        <th>Encryption</th>
                        <th>From Address</th>
                        <th>From Name</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($email as $email)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$email->driver}}</td>
                        <td>{{$email->host}}</td>
                        <td>{{$email->port}}</td>
                        <td>{{$email->username}}</td>
                        <td>{{$email->encryption}}</td>
                        <td>{{$email->from_address}}</td>
                        <td>{{$email->from_name}}</td>
                        <td>{{$email->status}}</td>
                        <td>
                          <a href="{{route('email.editdata', $email->id)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                          </a>
                          <form action="{{route('email.deletedata', $email->id)}}" id="delete{{$email->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                          @method('delete')
                          <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                          @csrf
                          </form>
                        </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 