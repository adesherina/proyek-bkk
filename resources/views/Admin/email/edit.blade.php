@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Email</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Email</a></div>
        <div class="breadcrumb-item">Form Update Email</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('email.update', $email->id)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Update Email</h4>
                <div class="card-header-action">
                  <a href="{{url('email/list')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Driver</label>
                  <input type="text" class="form-control" name="driver" value="{{$email->driver}}">
                  @if ($errors->has('driver'))
                      <span class="text-danger">{{ $errors->first('driver') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Host</label>
                  <input type="text" class="form-control" name="host" value="{{$email->host}}">
                  @if ($errors->has('host'))
                      <span class="text-danger">{{ $errors->first('host') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Port</label>
                  <input type="text" class="form-control" name="port" value="{{$email->port}}">
                  @if ($errors->has('port'))
                      <span class="text-danger">{{ $errors->first('port') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" value="{{$email->username}}">
                  @if ($errors->has('username'))
                      <span class="text-danger">{{ $errors->first('username') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="text" class="form-control" name="password" value="{{$email->password}}">
                  @if ($errors->has('password'))
                      <span class="text-danger">{{ $errors->first('password') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Encryption</label>
                  <input type="text" class="form-control" name="encryption" value="{{$email->encryption}}">
                  @if ($errors->has('encryption'))
                      <span class="text-danger">{{ $errors->first('encryption') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>From Address</label>
                  <input type="text" class="form-control" name="from_address" value="{{$email->from_address}}">
                  @if ($errors->has('from_address'))
                      <span class="text-danger">{{ $errors->first('from_address') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <label>From Name</label>
                  <input type="text" class="form-control" name="from_name" value="{{$email->from_name}}">
                  @if ($errors->has('from_name'))
                      <span class="text-danger">{{ $errors->first('from_name') }}</span>
                  @endif
                </div>
                <input type="hidden" name="status" value="{{$email->status}}">

                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 