@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Bidang Perusahaan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Bidang Perusahaan</a></div>
        <div class="breadcrumb-item">Form Update Bidang Perusahaan</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('bidang.update', $bidang->id)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Update Bidang Perusahaan</h4>
                <div class="card-header-action">
                  <a href="{{url('bidang/list')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Bidang</label>
                  <input type="text" class="form-control" name="nama_bidang" value="{{$bidang->nama_bidang}}">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Update</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 