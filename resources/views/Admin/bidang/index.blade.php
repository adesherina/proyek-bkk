@extends('Admin.master')
@section('title', 'Admin BKK')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Bidang Perusahaan</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Bidang Perusahaan</a></div>
                <div class="breadcrumb-item">Data Bidang Perusahaan</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @elseif(session('status2'))
                <div class="alert alert-danger">
                    {{ session('status2') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Bidang Perusahaan</h4>
                <div class="card-header-action">
                  <a href="{{route('bidang.create')}}" class="btn btn-success btn-lg">
                    Add
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Bidang</th>
                        <th>created at</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($bidang as $item)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$item->nama_bidang}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                          <a href="{{route('bidang.editdata', $item->id)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                          </a>
                          <form action="{{route('bidang.deletedata', $item->id)}}" id="delete{{$item->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                          @method('delete')
                          <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                          @csrf
                          </form>
                         
                        </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 