<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\Berkas;
use App\Model\Loker;
use Illuminate\Http\Request;
use App\Model\Pendaftar;
use Illuminate\Support\Facades\Auth;
use Throwable;

class PendaftaranController extends Controller
{
    public function save(Request $request)
    {
        $request->validate([
            'nik' => 'required',
            'no_telp' => 'required',
            'agama' => 'required'
        ]);

        try {

            if (is_null(Auth::id())) {
                return redirect()->back()->with('failed', 'Harap login terlebih dahulu');
            }

            $pd = new Pendaftar();
            $pd->user_id = Auth::id();
            $pd->loker_id = $request->loker_id;
            $pd->nik = $request->nik;
            $pd->no_telp = $request->no_telp;
            $pd->status = "unverified";
            $pd->agama = $request->agama;
            $pd->save();

            return redirect()->route('user.pendaftaran.confirmation', $pd->id);
        } catch (Throwable $e) {
            return $e;
        }
    }

    public function confirmation($id)
    {
        $loker = Pendaftar::find($id);
        return view('User.lowonganKerja.confirmation', compact('loker'));
    }

    public function uploadFile(Request $request)
    {
        if ($request->hasFile('dokumen_pribadi') && $request->hasFile('dokumen_persyaratan')) {
            $resorcePribadi = $request->file('dokumen_pribadi');
            $namePribadi   = $resorcePribadi->getClientOriginalName();
            $resorcePribadi->move(\base_path() . "/public/berkas-pendaftaran", $namePribadi);

            $resorcePersyaratan = $request->file('dokumen_persyaratan');
            $namePersyaratan   = $resorcePersyaratan->getClientOriginalName();
            $resorcePersyaratan->move(\base_path() . "/public/berkas-pendaftaran", $namePersyaratan);

            $pd = Pendaftar::find($request->pendaftaran_id);

            try {
                $bk = new Berkas();
                $bk->berkas_pribadi = $namePribadi;
                $bk->berkas_persyaratan = $namePersyaratan;
                $bk->pendaftaran_id = $request->pendaftaran_id;
                $bk->save();

                return redirect()->route('lowongan.detail', $pd->lokerRef->id)->with('success', 'Upload berkas berhasil, silahkan ke lokasi bkk untuk konfirmasi');
            } catch (Throwable $e) {
                return $e;
            }
        }
    }
}
