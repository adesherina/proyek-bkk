<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\DataUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class forgotPassword extends Controller
{
    //tampil forgot pw
    public function index(){
        return view('user.forgotPassword.cariEmail');
    }

    //forgot pw dengan mencari email
    public function cariEmail(Request $request)
    {
        $user = DataUser::whereemail($request->email)->first();

        if ($user != null){
            return view('user.forgotPassword.forgotpw', ['email' =>$request->email]);
        } else {
            return view('user.forgotPassword.cariEmail')->with('failed', 'Email Tidak Terdaftar');
        }

        
    }

    //forgot pw
    public function forgotPw(Request $request)
    {
        $newPassword = $request->newPassword;
        $email = $request->email;
        $confirmPassword = $request->confirmPassword;
        //    dd($newPassword, $confirmPassword);
        if ($newPassword === $confirmPassword){
            $change = DB::table('users')
            ->where('email', $email)
            ->update(['password' => Hash::make($newPassword)]);
            
            return redirect('user/login')->with('success', 'Berhasil Mengganti Password');
        } elseif($newPassword != $confirmPassword){

            return view('user.forgotPassword.cariEmail', ['email'=>$email])->with('failedPass', 'Password Konfirmasi Tidak Sama');
        }
    }
}
