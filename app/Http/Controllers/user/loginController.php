<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\UserLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class loginController extends Controller
{
    

    //tampil login
    public function index(){
        return view('User.login');
    }

    public function login(Request $request){

        $user = User::where('email', $request->email)->first();
        if($user != null){
            if(Hash::check($request->password, $user->password)){
                if((bool)$user->is_verified != false){
                    Auth::loginUsingId($user->id);
                    return redirect('/');
                }else{
                    return redirect('user/login')->with('message2','Anda belum verifikasi email');
                }
            }else{
                 return redirect('user/login')->with('message2','password salah');
            }
        }else{
             return redirect('user/login')->with('message2','Akun Anda tidak ada');
        }
    }

    //logout
    public function logout(Request $request){
        $request->session()->flush();
        return redirect('user/login');
    }
}
