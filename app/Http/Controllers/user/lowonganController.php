<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Loker;
use App\Model\Timeline;
use App\Model\Bidang;
use App\Model\Perusahaan;
use Illuminate\Http\Request;

class lowonganController extends Controller
{
    //tampil lowongan
    public function lowongan()
    {
        $loker = Loker::paginate(15);
        $bidang = Bidang::all();
        return view('User.lowonganKerja.listLowongan', compact('loker', 'bidang'));
    }

    public function detail($id)
    {
        
        $tl = Timeline::where('loker_id', $id)->get();
        $loker = Loker::find($id);
        return view('User.lowonganKerja.detail', compact('loker', 'tl'));
    }

    public function searchFunc(Request $request)
    {
        $keyword = $request->name;
        $bidang = $request->bidang;
        $minKuota = $request->min;
        $maxKuota = $request->max;
        $status = $request->status;

        $loker = Loker::when($keyword, function ($q) use ($keyword) {
                return $q->where('nama_loker', 'like', '%' . $keyword . '%');
            })
            ->when($bidang, function ($q) use ($bidang) {
                if ($bidang != "null") {
                    return $q->where('bidang', $bidang);
                }
            })
            ->when($status, function ($q) use ($status) {
                return $q->where('status', [$status]);
            })
            ->when($minKuota, function ($q) use ($minKuota, $maxKuota) {
                return $q->whereBetween('kuota', [$minKuota, $maxKuota]);
            })
            ->paginate(15);

        $bidang = Bidang::all();
        $bidangFilter = Bidang::where('id', $bidang)->first();
        return view('User.lowonganKerja.listLowongan', compact('loker', 'bidang', 'keyword', 'minKuota', 'maxKuota', 'bidangFilter','status'));
    }

    public function download($id)
    {
        try {
            $loker = Loker::where('id', $id)->firstOrFail();
            $file = public_path() . "\\formulir_pendaftaran\\" . $loker->formulir_pendaftaran;
            return response()->download($file);
        } catch (Throwable $e) {
            return redirect()->back()->with('failed', 'Berkas tidak ada');
        }
    }
}
