<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Loker;
use Illuminate\Http\Request;

class berandaController extends Controller
{
    //tampil beranda
    public function beranda()
    {
        $loker = Loker::orderBy('created_at', 'DESC')->limit('4')->get();
        return view('User.beranda', compact('loker'));
    }
}
