<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Mail\UserVerification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class registerController extends Controller
{
    //tampil register
    public function index(){
        return view('User.register');
    }

    //add proses register
    public function addProcess(Request $request)
    {
        $timestamps = true;
        //query builder insert
        $rules = [
            'nama_lengkap' => 'required|min:5|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5'
        ];
 
        $messages = [
            'nama_lengkap.required'          => 'Nama wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 5 karakter.',
            'email.required'         => 'Email wajib diisi.',
            'email.email'            => 'Email tidak valid.',
            'email.unique'           => 'Email sudah terdaftar.',
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        if($request->password != $request->confirmPassword){
             return redirect()->route('user.register')->with('statusFailed', 'Password Konfirmasi Tidak Sama');
        }else{
            $token = Str::random();
            $user = new User;
            $user->nama_lengkap = $request->nama_lengkap;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->email_token = $token;
            $user->is_verified = 0;


            $user->save();
            //
            if($user->save()){
                $data = ([
                    'nama' => $request->nama_lengkap,
                    'email' => $request->email,
                    'token' => $token
                ]);
                //kirim email ke user yg register
                Mail::to($request->email)->send(new UserVerification($data));
                return redirect()->route('user.login')->with('message', 'Register Berhasil, silahkan konfirmasi email');
            }else{
                return redirect()->route('user.register')->with('message2', 'Register akun gagal');
            }
        }
        
    }
    public function verification($token){
        //cari data user berdasarkan emao
        $verif = User::where("email_token", $token)->first();
        //
        if($verif != null){
            User::where("email_token", $token)->update([
                'email_verified_at' => date("Y-m-d\TH:i:s"),
                'is_verified' => 1
            ]);
            return view('user.login')->with('statusRegister', 'Konfirmasi email berhasil');
        }
    }
}
