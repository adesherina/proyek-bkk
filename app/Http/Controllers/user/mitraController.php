<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Bidang;
use App\Model\Loker;
use App\Model\Perusahaan;
use Illuminate\Http\Request;

class mitraController extends Controller
{
    //tampil lowongan
    public function index()
    {
        $ps = Perusahaan::paginate(15);
        $bidang = Bidang::all();
        return view('User.mitra.index', compact('ps', 'bidang'));
    }

    public function detail($id)
    {
        $ps = Perusahaan::where('id', $id)->with('bidangs')->first();
        $loker = Loker::where('pt_id', $id)->whereStatus('open')->get();
        if ($ps) {
            return view('User.mitra.detail', compact('ps', 'loker'));
        }
    }

    public function sortBy($value)
    {
        if ($value === "none") {
            $ps = Perusahaan::paginate(15);
        } elseif ($value === "terbaru") {
            $ps = Perusahaan::orderBy('created_at', 'DESC')->paginate(15);
        } elseif ($value === "bidang") {
            $ps = Perusahaan::orderBy('bidang', 'ASC')->paginate(15);
        } else {
            $ps = Perusahaan::orderBy('nama_pt', 'ASC')->paginate(15);
        }


        foreach ($ps as $item) {
            $url = url('foto-PT/' . $item->photo);
            echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">';
            echo '<div class="single-services text-center mb-30 " style="padding: 5px">';
            echo '<div class="services-ion">';
            echo '<img src="' . $url . '" style="width: 100%; height:200px" alt="">';
            echo '</div>';
            echo '<div class="services-cap ml-2 mt-3 mb-3 text-left">';
            $route = route('mitra.detail', $item->id);
            echo '<h5><a href="' . $route . '"><i class="fa fa-building mr-2" aria-hidden="true"></i>' . $item->nama_pt . '</a></h5>';
            echo '<span><i class="fa fa-cog mr-2" aria-hidden="true"></i>' . $item->bidangs->nama_bidang . '</span>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        };
    }

    public function searchFunc(Request $request)
    {
        $keyword = $request->name;
        $bidang = $request->bidang;

        $ps = Perusahaan::when($keyword, function ($q) use ($keyword) {
            return $q->where('nama_pt', 'like', '%' . $keyword . '%');
        })
            ->when($bidang, function ($q) use ($bidang) {
                if ($bidang != "all") {
                    return $q->where('bidang', $bidang);
                }
            })
            // ->when($minKuota, function ($q) use ($minKuota, $maxKuota) {
            //     return $q->whereBetween('kuota', [$minKuota, $maxKuota]);
            // })
            ->paginate(15);

        $bidang = Bidang::all();
        $bidangFilter = Bidang::where('id', $bidang)->first();
        return view('User.mitra.index', compact('ps', 'bidang', 'keyword', 'bidangFilter'));
    }
}
