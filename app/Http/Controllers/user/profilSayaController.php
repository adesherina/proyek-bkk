<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Pendaftar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class profilSayaController extends Controller
{
    //tampil update profil
    public function update($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        return view('User.profil-user.profilSaya', compact('user'));
    }

    //simpan update profil
    public function updateProcess(Request $request, $id)
    {
        if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/user-profil", $name);
            // dd($request->all());
            DB::table('users')->where('id', Auth::id())->update(
                [
                    'nama_lengkap' => $request->nama_lengkap,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $name
                ]
            );
            return redirect()->route('user.update', $id)->with('statusSuccess1', 'Data Berhasil Di Update');
        } else {
            DB::table('users')->where('id', Auth::id())->update(
                [
                    'nama_lengkap' => $request->nama_lengkap,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $request->oldPhoto
                ]
            );
            return redirect()->route('user.update', $id)->with('statusSuccess2', 'Data Berhasil Di update');
        }
    }

    //tampil update Password
    public function updatePw($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        return view('User.profil-user.ubahPassword', compact('user'));
    }

    //Update password
    public function updatePassword(Request $request, $id)
    {

        $newPassword = $request->newPassword;
        if ($newPassword === $request->confirmPassword) {
            DB::table('users')->where('id', $id)->update([
                'password' => Hash::make($request->newPassword),
            ]);
            return redirect()->back()->with("editpw", "Berhasil Ganti Password");
        } else {
            return redirect()->back()->with("failedpw", "Gagal Ganti Password");
        }
    }

    public function listLamaran()
    {
        $lm = Pendaftar::with('lokerRef')->where('user_id', Auth::id())->get();
        return view('User.profil-user.lamaran', compact('lm'));
    }

    public function statusLamaran($status)
    {
        if ($status != "all") {
            $lm = Pendaftar::with('lokerRef')->where('user_id', Auth::id())->where('status', $status)->get();
        } else {
            $lm = Pendaftar::with('lokerRef')->where('user_id', Auth::id())->get();
        }

        if ($lm->count() > 0) {
            foreach ($lm as $item) {
                echo '<div class="single-job-items mb-30">';
                echo '<div class="job-items">';
                echo '<div class="company-img">';
                $url = url('foto-PT/' . $item->lokerRef->perusahaan->photo);
                echo '<a href="#"><img src="' . $url . '" style="max-width: 70px; 7eight:60px" alt=""></a>';
                echo '</div>';
                echo '<div class="job-tittle job-tittle2">';
                echo '<a href="' . route('lowongan.detail', $item->lokerRef->id) . '">';
                echo '<h4>' . $item->lokerRef->nama_loker . '</h4>';
                echo '</a>';
                echo '<ul>';
                echo '<li>' . $item->lokerRef->perusahaan->nama_pt . '</li>';
                echo '<li>' . $item->lokerRef->range_gaji . '</li>';
                echo '</ul>';
                echo '</div>';
                echo '</div>';
                echo '<div class="items-link items-link2 f-right">';
                echo '<a href="#" href="javascript:void(0);">' . $item->status . '</a>';
                echo '<span>update ' . $item->updated_at->diffForHumans() . '</span>';
                echo '</div>';
                echo '</div>';
            };
        } else {
            echo '<p class="h4 font-weight-bold">Data Tidak Tersedia</p>';
        }
    }
}
