<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class profilController extends Controller
{
    //tampil Profil
    public function profil(){
        return view('User.profilBKK.profil');
    }

    //tampil Kontak
    public function kontak(){
        return view('User.profilBKK.kontak');
    }
}
