<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class GoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackHandle()
    {

        // jika user masih login lempar ke login
        if (Auth::check()) {
            return redirect('user/login');
        }

        $oauthUser = Socialite::driver('google')->stateless()->user(); //ngambil data gmail
        $user = User::where('google_id', $oauthUser->id)->first();

        //jika user sudah mendaftar dgn google 
        if ($user) {
            Auth::loginUsingId($user->id);

            return redirect('/');
        } else {
            // jika user baru pertama kali login
            $newUser = User::create([
                'nama_lengkap' => $oauthUser->name,
                'username' => $oauthUser->name . "_bkk",
                'email' => $oauthUser->email,
                'google_id' => $oauthUser->id,
                // password tidak akan digunakan
                'password' => md5($oauthUser->token),
                'is_verified' => 1,
            ]);
            Auth::login($newUser);
            return redirect('/');
        }
    }
}
