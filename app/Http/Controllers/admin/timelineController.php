<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Timeline;
use Facade\FlareClient\Time\Time;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class timelineController extends Controller
{
    //tampil data
    public function index(){
        $timeline = Timeline::with('lokerRef')
        ->get();
        return view('Admin.timeline.list', compact('timeline'));
    }

    //tambah data
    public function tambah(){
        return view('Admin.timeline.tambah');
    }

    //simpan data
    public function simpan(Request $request){
        
        $rules = [
            'loker_id' => 'required',
            'deskripsi' => 'required|min:10',
            'tanggal_pembukaan' => 'required',
            'tanggal_penutupan' => 'required',
        ];
 
        $messages = [
            'nama_loker.required'  => 'Nama Loker  wajib diisi',
            'deskripsi.required'  => 'Deskripsi  wajib diisi',
            'deskripsi.min:10'  => 'Deskripsi minimal 10 karakter',
            'tanggal_pembukaan.required'  => 'Tanggal Pembukaan  wajib diisi',
            'tanggal_penutupan.required'  => 'Tanggal Penutupan  wajib diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        try{
            $timeline = new Timeline();
            $timeline->loker_id = $request->loker_id;
            $timeline->deskripsi = $request->deskripsi;
            $timeline->tanggal_pembukaan = $request->tanggal_pembukaan;
            $timeline->tanggal_penutupan = $request->tanggal_penutupan;
            $timeline->save();
            
            return redirect('timeline/list')->with('status', 'Data Berhasil Ditambahkan');
        }catch(Throwable $e){
            return redirect('timeline/list')->with('status', 'Data Gagal Ditambahkan');
        }
 
        
    }

    //edit data
    public function edit($id){
        $timeline = Timeline::with('lokerRef')
        ->where('id', $id)
        ->first();
        return view('Admin.timeline.edit', compact('timeline'));

    }

    //update
    public function update(Request $request, $id)
    {
        $timeline = Timeline::find($request->id);
        $timeline->loker_id = $request->loker_id;
        $timeline->deskripsi = $request->deskripsi;
        $timeline->tanggal_pembukaan = $request->tanggal_pembukaan;
        $timeline->tanggal_penutupan = $request->tanggal_penutupan;
        $timeline->save();
        return redirect('timeline/list')->with('status', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('timeline')->where('id', $id)->delete();
        
        return redirect('timeline/list')->with('status', 'Data Berhasil Dihapus');
    }

    //detail data
    public function detail($id){
        $timeline = Timeline::with('lokerRef')->where('id', $id)->first();
       
        return view('Admin.timeline.detail', compact('timeline'));

    }
}
