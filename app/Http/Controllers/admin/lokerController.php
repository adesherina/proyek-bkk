<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Bidang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Model\Loker;
use Illuminate\Support\Facades\Validator;
use Throwable;

class lokerController extends Controller
{
    //tampil data
    public function index()
    {
        $loker = Loker::with('perusahaan')
            ->with('bidangs')
            ->get();
        return view('Admin.loker.list', compact('loker'));
    }

    //tambah data
    public function tambah()
    {
        return view('Admin.loker.tambah');
    }

    //simpan data
    public function simpan(Request $request)
    {
        $rules = [
            'nama_loker' => 'required',
            'pt_id' => 'required',
            'bidang' => 'required',
            'sk' => 'required|min:10',
            'jenis_kelamin' => 'required',
            'deskripsi' => 'required|min:10',
            'kuota' => 'required',
            'batas_akhir' => 'required',
            'formulir_pendaftaran'=>'required'
        ];

        $messages = [
            'nama_loker.required'  => 'Nama Loker  wajib diisi',
            'pt_id.required'  => 'Nama Perusahaan wajib diisi',
            'bidang.required'         => 'Bidang wajib diisi',
            'sk.required'      => 'Syarat dan Ketentuan wajib diisi',
            'sk.min:10'      => 'Syarat dan Ketentuan minimal 10 karakter',
            'jenis_kelamin.required'           => 'Jenis Kelamin wajib diisi',
            'deskripsi.required'  => 'Deskripsi  wajib diisi',
            'deskripsi.min:10'  => 'Deskripsi minimal 10 karakter',
            'kuota.required'  => 'Kuota  wajib diisi',
            'batas_akhir.required'  => 'Batas Akhir  wajib diisi',
            'formulir_pendaftaran.required'  => 'Formulir Pendaftaran wajib diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        try {
            if ($request->hasFile('formulir_pendaftaran')) {
            $resorce = $request->file('formulir_pendaftaran');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/formulir_pendaftaran", $name);

                $loker = new Loker();
                $loker->nama_loker = $request->nama_loker;
                $loker->pt_id = $request->pt_id;
                $loker->bidang = $request->bidang;
                $loker->jenis_kelamin = $request->jenis_kelamin;
                $loker->sk = $request->sk;
                $loker->deskripsi = $request->deskripsi;
                $loker->kuota = $request->kuota;
                $loker->batas_akhir = $request->batas_akhir;
                $loker->range_gaji = $request->range_gaji;
                $loker->formulir_pendaftaran = $name;
                $loker->status = "open";
                $loker->save();
            echo "Berkas berhasil di upload";
            return redirect('loker/list')->with('status', 'Data Berhasil Ditambahkan');
        } else {
            echo "Gagal upload Berkas";
            return redirect('loker/list')->with('status', 'Data Gagal Ditambahkan');
        }
            // return redirect('loker/list')->with('status', 'Data Berhasil Ditambahkan');
        } catch (Throwable $e) {
            return $e;
            // return redirect('loker/list')->with('status', 'Data Gagal Ditambahkan');
        }
    }

    //edit data
    public function edit($id){
        $loker = Loker::with('perusahaan','bidangs')
        ->where('id', $id)
        ->first();
        return view('Admin.loker.edit', compact('loker'));
    }

    //update
    public function update(Request $request, $id)
    {
        $loker = Loker::find($request->id);
        $loker->nama_loker = $request->nama_loker;
        $loker->pt_id = $request->pt_id;
        $loker->bidang = $request->bidang;
        $loker->jenis_kelamin = $request->jenis_kelamin;
        $loker->sk = $request->sk;
        $loker->deskripsi = $request->deskripsi;
        $loker->kuota = $request->kuota;
        $loker->batas_akhir = $request->batas_akhir;
        $loker->save();
        return redirect('loker/list')->with('status', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('loker')->where('id', $id)->delete();

        return redirect('loker/list')->with('status', 'Data Berhasil Dihapus');
    }

    //detail data
    public function detail($id)
    {
        $loker = Loker::with('perusahaan')->where('id', $id)->first();

        return view('Admin.loker.detail', compact('loker'));
    }
}
