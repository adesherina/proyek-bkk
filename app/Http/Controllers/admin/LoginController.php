<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Model\Login;

class LoginController extends Controller
{
    public function index(){
        return view('Admin.login');
    }

    public function dashboard(){
        return view('Admin.dashboard');
    }

    public function login(Request $request){
        // dd($request->all());

        $data = Login::where('username', $request->username)->first();
        // dd($data);
        if (!$data){
            return redirect('admin/login')->with('message', 'username salah');
        }else{
            if(Hash::check($request->password, $data->password)){
                Session::put('name',$data->nama);
                Session::put('id',$data->id);

                session(['berhasil_login' => true]);
                return redirect('/dashboard');
            }
            return redirect('admin/login')->with('message', 'password salah');
        }
        
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('admin/login');
    }
}
