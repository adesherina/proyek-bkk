<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(){
        return view('Admin.register');
    }

    public function addProcess(Request $request)
    {
        $timestamps = true;
        //query builder insert
        DB::table('admin')->insert(
            [
                'nama' => $request->nama,
                'username' => $request->username,
                'password' => Hash::make($request->password),
            ]
        );
        //Redirect dengan status 
        return redirect('admin/login')->with('status', 'Data Berhasil Ditambahkan');
    }
}
