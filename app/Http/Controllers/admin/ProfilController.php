<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class ProfilController extends Controller
{
    //tampil profil
    public function index($id)
    {
        $admin = DB::table('admin')->where('id', $id)->first();
        // dd($admin);

        return view('Admin.profil.akun', compact('admin'));
    }

    //simpan update profil
    public function updateProcess(Request $request, $id){
        if($request->hasFile('photo')){
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/admin-profil", $name);
            DB::table('admin')->where('id', Session::get('id') )->update(
                [
                    'nama' => $request->nama,
                    'no_telp'=>$request->no_telp,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $name
                ]);
            return redirect('admin/profil/'.$id)->with('statusSuccess1', 'Data Berhasil Di Update');
        }else{
             DB::table('admin')->where('id', Session::get('id') )->update(
                [
                    'nama' => $request->nama,
                    'no_telp'=>$request->no_telp,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $request->oldPhoto
                ]);
            return redirect('admin/profil/'.$id) ->with('statusSuccess2', 'Data Gagal Di update');
        }   
    }

    //tampil update Password
    public function updatePw($id){
        $admin = DB::table('admin')->where('id', $id)->first();
        return view('Admin.profil.ubahPassword', compact('admin'));

    }

    //Update password
    public function updatePassword(Request $request, $id)
    {
        
        $newPassword = $request->newPassword;
        if ($newPassword === $request->confirmPassword) {
            DB::table('admin')->where('id', $id)->update([
                'password' => Hash::make($request->newPassword),
            ]);
            return redirect()->back()->with("editpw", "Berhasil Ganti Password");
        } else {
           return redirect()->back()->with("failedpw", "Gagal Ganti Password");
        }
    }
}
