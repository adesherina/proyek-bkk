<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Bidang;
use App\Model\Perusahaan;
use Illuminate\Support\Facades\DB;

class perusahaanController extends Controller
{
    //tampil data
    public function index()
    {
        $pt = Perusahaan::with('bidangs')->get();
        return view('Admin.PT.list', compact('pt'));
    }

    //tambah data
    public function tambah()
    {
        $bidang = Bidang::all();
        return view('Admin.PT.tambah', compact('bidang'));
    }

    //simpan data
    public function simpan(Request $request)
    {
        if ($request->hasFile('photo') && $request->hasFile('banner')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/foto-PT", $name);

            $resorce2 = $request->file('banner');
            $name2   = $resorce2->getClientOriginalName();
            $resorce2->move(\base_path() . "/public/banner-PT", $name2);

            $ps = new Perusahaan();
            $ps->nama_pt = $request->nama_pt;
            $ps->bidang = $request->bidang;
            $ps->no_telp = $request->no_telp;
            $ps->email = $request->email;
            $ps->deskripsi = $request->deskripsi;
            $ps->alamat = $request->alamat;
            $ps->photo = $name;
            $ps->map = $request->map;
            $ps->gambar_banner = $name2;
            $ps->save();

            echo "Foto berhasil di upload";
            return redirect('pt/list')->with('status', 'Data Berhasil Ditambahkan');
        } else {
            echo "Gagal upload Foto";
            return redirect('pt/list')->with('status2', 'Data Gagal Ditambahkan');
        }

    }

    //edit data
    public function edit($id)
    {
        $pt = Perusahaan::where('id', $id)->first();
        return view('Admin.pt.edit', ['pt' => $pt]);
    }

    //update
    public function update(Request $request, $id)
    {
        if ($request->hasFile('photo') && $request->hasFile('banner')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/foto-PT", $name);

            $resorce2 = $request->file('banner');
            $name2   = $resorce2->getClientOriginalName();
            $resorce2->move(\base_path() . "/public/banner-PT", $name2);

            $ps = Perusahaan::find($request->id);
            $ps->nama_pt = $request->nama_pt;
            $ps->bidang = $request->bidang;
            $ps->no_telp = $request->no_telp;
            $ps->email = $request->email;
            $ps->deskripsi = $request->deskripsi;
            $ps->alamat = $request->alamat;
            $ps->photo = $name;
            $ps->map = $request->map;
            $ps->gambar_banner = $name2;
            $ps->save();

            echo "Foto berhasil di upload";
            return redirect('pt/list')->with('status', 'Data Berhasil Ditambahkan');
        } else {
            echo "Gagal upload Foto";
            return redirect('pt/list')->with('status2', 'Data Gagal Ditambahkan');
        }
    }

    //hapus data
    public function delete($id)
    {
        DB::table('pt')->where('id', $id)->delete();

        return redirect('pt/list')->with('status', 'Data Berhasil Dihapus');
    }

    //detail data
    public function detail($id)
    {
        $pt = DB::table('pt')->where('id', $id)->first();
        return view('Admin.pt.detail', ['pt' => $pt]);
    }
}
