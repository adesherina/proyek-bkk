<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Email;
use Illuminate\Support\Facades\DB;


class emailKonfigController extends Controller
{
    //tampil data
    public function index(){
        $email = Email::all();
        return view('Admin.email.list', ['email'=> $email]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.email.tambah');
    }

    //simpan data
    public function simpan(Request $request){
        Email::create([
             'driver'=>$request->driver,
                    'host' => $request->host,
                    'port' => $request->port,
                    'username' => $request->username,
                    'password' => $request->password,
                    'encryption' => $request->encryption,
                    'from_address' => $request->from_address,
                    'from_name' => $request->from_name,
                    'status' => 0,
        ]);
        return redirect('email/list')->with('status', 'Data Berhasil Ditambahkan');

    }

    //edit data
    public function edit($id){
        $email = Email::find($id);
        return view('Admin.email.edit', ['email'=> $email]);

    }

    //update
    public function update(Request $request, $id)
    {
        
        Email::where('id', $id)->update([
            'driver'=>$request->driver,
                'host' => $request->host,
                'port' => $request->port,
                'username' => $request->username,
                'password' => $request->password,
                'encryption' => $request->encryption,
                'from_address' => $request->from_address,
                'from_name' => $request->from_name,
                'status' => $request->status,
        ]);
        return redirect('email/list')->with('status', 'Data Berhasil Diedit');

    }

    //hapus data
    public function delete($id)
    {
        DB::table('emails')->where('id', $id)->delete();
        
        return redirect('email/list')->with('status', 'Data Berhasil Dihapus');
    }

    
}
