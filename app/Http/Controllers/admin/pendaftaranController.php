<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Berkas;
use App\Model\Pendaftar;
use Illuminate\Http\Request;
use Throwable;

class pendaftaranController extends Controller
{
    //tampil data
    public function index()
    {
        $pd = Pendaftar::with('userRef', 'lokerRef')->get();
        return view('Admin.pendaftaran.list', compact('pd'));
    }

    //detail data
    public function detail($id)
    {
        $pendaftaran = Pendaftar::with('userRef', 'lokerRef')->where('id', $id)->first();

        return view('Admin.pendaftaran.detail', compact('pendaftaran'));
    }

    public function downloadPribadi($id)
    {
        try {
            $bk = Berkas::where('pendaftaran_id', $id)->firstOrFail();
            $file = public_path() . "\\berkas-pendaftaran\\" . $bk->berkas_pribadi;
            return response()->download($file);
        } catch (Throwable $e) {
            return redirect()->back()->with('failed', 'Berkas tidak ada');
        }
    }

    public function downloadPersyaratan($id)
    {
        try {
            $bk = Berkas::where('pendaftaran_id', $id)->firstOrFail();
            $file = public_path() . "\\berkas-pendaftaran\\" . $bk->berkas_persyaratan;
            return response()->download($file);
        } catch (Throwable $e) {
            return redirect()->back()->with('failed', 'Berkas tidak ada');
        }
    }

    public function makeLunas($id)
    {
        $pr = Pendaftar::find($id);
        $pr->registrasi = 'lunas';
        $pr->save();

        $success['status'] = '1';
        $success['message'] = 'Config save success';
        return response()->json($success, 200);
    }

    public function makePending($id)
    {
        $pr = Pendaftar::find($id);
        $pr->registrasi = 'pending';
        $pr->save();

        $success['status'] = '1';
        $success['message'] = 'Config save success';
        return response()->json($success, 200);
    }

    public function changeStatus(Request $request)
    {
        $pr = Pendaftar::find($request->id);
        $pr->status = $request->status;
        $pr->save();

        $success['status'] = '1';
        $success['message'] = 'Config save success';
        return response()->json($success, 200);
    }
}
