<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class UserAdminController extends Controller
{
    //Tampil Data User
    public function data()
    {
        $users = DB::table('users')->get();

        return view('Admin.user.user', ['users' => $users]);
    }
    
    //Delete Data User
    public function delete($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect('datauser')->with('status', 'Data Berhasil Dihapus');
    }

    //Tampil Update User
    public function edit($id){
        $user = DB::table('users')->where('id', $id)->first();
        return view('Admin.user.edit', ['user'=> $user]);

    }

    //Proses Update Data User
    public function update(Request $request, $id)
    {
        if($request->hasFile('photo')){
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/foto-user", $name);
             
            DB::table('users')
            ->where('id', $id)
            ->update(
                [
                'nama_lengkap'=>$request->nama_lengkap,
                'username' => $request->username,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tb' => $request->tb,
                'bb' => $request->bb,
                'tahun_lulus' => $request->tahun_lulus,
                'photo' => $name
                ]);
        }else{
            DB::table('users')
            ->where('id', $id)
            ->update(
                [
                'nama_lengkap'=>$request->nama_lengkap,
                'username' => $request->username,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tb' => $request->tb,
                'bb' => $request->bb,
                'tahun_lulus' => $request->tahun_lulus,
                'photo' => $request->oldPhoto
                ]);
        }
        return redirect('datauser')->with('status', 'Data Berhasil Diedit');
    }

    //Tampil Detail Data User
    public function detail($id){
        $user = DB::table('users')->where('id', $id)->first();
        return view('Admin.user.detail', ['user'=> $user]);

    }
}