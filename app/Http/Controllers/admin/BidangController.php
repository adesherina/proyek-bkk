<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Bidang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class BidangController extends Controller
{
    //tampil list
    public function index()
    {
        $bidang = Bidang::all();
        return view('admin.bidang.index', compact('bidang'));
    }

    //tampil tambah bidang
    public function create()
    {
        return view('admin.bidang.create');
    }

    //proses tambah bidang
    public function save(Request $request)
    {
        try {
            $bidang = new Bidang();
            $bidang->nama_bidang = $request->nama_bidang;
            $bidang->save();

            return redirect()->route('bidang.index')->with('status', 'Berhasil menambahkan bidang');
        } catch (Throwable $e) {
            return redirect()->route('bidang.index')->with('status2', 'Berhasil menambahkan bidang');
        }
    }

    //edit data
    public function edit($id){
        $bidang = DB::table('bidang')->where('id', $id)->first();
        return view('Admin.bidang.edit', compact('bidang'));

    }

    //update
    public function update(Request $request, $id)
    {
        $bidang = Bidang::find($request->id);
        $bidang->nama_bidang = $request->nama_bidang;
        $bidang->save();
        return redirect('bidang/list')->with('status', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('bidang')->where('id', $id)->delete();

        return redirect('bidang/list')->with('status', 'Data Berhasil Dihapus');
    }
}
