<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLogin extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = ['name', 'email', 'password','timestamps',];
    protected $hidden = [
        'password', 
    ];
}
