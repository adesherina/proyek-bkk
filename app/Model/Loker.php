<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Perusahaan;
use Carbon\Carbon;
use App\Model\Bidang;
use Illuminate\Support\Str;

class Loker extends Model
{
    //
    protected $table = 'loker';
    protected $timestamp = true;
    protected $guarded = [];
    protected $dates = ['batas_akhir'];

    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, 'pt_id', 'id');
    }

    public function bidangs()
    {
        return $this->hasOne(Bidang::class, 'id', 'bidang');
    }

    public function getDeskripsiExcerptAttribute()
    {
        return Str::words($this->deskripsi, '15');
    }

    public function getBatasAkhirAttribute($value)
    {
        $value = Carbon::parse($value);
        return $value->translatedFormat('d M Y');
    }
}
