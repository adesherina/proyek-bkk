<?php

namespace App\Model;

use App\User;
use App\Model\Loker;

use Illuminate\Database\Eloquent\Model;

class Pendaftar extends Model
{
    protected $table = "pendaftaran";

    public function userRef()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function lokerRef()
    {
        return $this->belongsTo(Loker::class, 'loker_id', 'id');
    }
}
