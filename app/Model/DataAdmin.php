<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataAdmin extends Model
{
       protected $table = "admin";

    public $timestamp = true;

    protected $guarded = ['update_at'];
}
