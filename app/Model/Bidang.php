<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Perusahaan;

class Bidang extends Model
{
    //
    protected $table = "bidang";
    public $timestamps = true;

    // public function perusahaan()
    // {
    //     return $this->belongsTo(Perusahaan::class, 'bidang', 'id');
    // }
}
