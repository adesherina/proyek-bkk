<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Pendaftar;

class Berkas extends Model
{
    protected $table = "berkas_pendaftaran";
    public $timestamps = true;

    public function registerRef()
    {
        return $this->hasOne(Pendaftar::class, 'pendaftaran_id', 'id');
    }
}
