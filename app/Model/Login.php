<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    protected $table = "admin";
    protected $fillable = ['nama', 'username', 'password','timestamps',];
    protected $hidden = [
        'password',
    ];
}
