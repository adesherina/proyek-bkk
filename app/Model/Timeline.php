<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Loker;

class Timeline extends Model
{
    protected $table = "timeline";
    public function lokerRef()
    {
        return $this->belongsTo(Loker::class, 'loker_id', 'id');
    }
}
