<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Bidang;
use Illuminate\Support\Str;

class Perusahaan extends Model
{
    //
    protected $table = "pt";
    public $timestamps = true;

    public function bidangs()
    {
        return $this->hasOne(Bidang::class, 'id', 'bidang');
    }

    public function getDeskripsiExcerptAttribute()
    {
        return Str::words($this->deskripsi, '15');
    }
}
