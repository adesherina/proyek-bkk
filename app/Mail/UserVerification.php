<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->nama = $data['nama'];
        $this->email = $data['email'];
        $this->token = $data['token'];

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user_email_verif')->subject('Verifikasi Email')->with([
            'nama' => $this->nama,
            'email' => $this->email,
            'token' => $this->token
        ]);
    }
}
