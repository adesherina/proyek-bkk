<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeline', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loker_id');
            $table->unsignedBigInteger('pt_id');
            $table->date('tanggal_pembukaan');
            $table->date('tanggal_penutupan');
            $table->text('deskripsi');
            $table->timestamps();

            $table->foreign('loker_id')->references('id')->on('loker')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pt_id')->references('id')->on('pt')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeline');
    }
}
