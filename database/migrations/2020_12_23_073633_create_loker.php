<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loker', function (Blueprint $table) {
            $table->id();
            $table->string('nama_loker', 100);
            $table->unsignedBigInteger('pt_id');
            $table->string('bidang', 50);
            $table->char('range_gaji',100);
            $table->string('gambar_banner', 255);
            $table->json('jenis_kelamin');
            $table->text('sk');
            $table->integer('kuota');
            $table->date('batas_akhir');
            $table->timestamps();

            $table->foreign('pt_id')->references('id')->on('pt')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loker');
    }
}
