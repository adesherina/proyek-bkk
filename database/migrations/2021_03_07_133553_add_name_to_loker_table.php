<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNameToLokerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loker', function (Blueprint $table) {
            //
            $table->text('deskripsi')->after('sk');
            $table->enum('status', ['open', 'closed', 'arsip'])->after('deskripsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loker', function (Blueprint $table) {
            $table->dropColumn('nama_loker');
            $table->dropColumn('deskripsi');
            $table->dropColumn('status');
        });
    }
}
