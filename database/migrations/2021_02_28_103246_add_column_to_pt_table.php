<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToPtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pt', function (Blueprint $table) {
            //
            $table->string('no_telp', 20)->after('alamat');
            $table->string('bidang', 100)->after('no_telp');
            $table->string('email', 100)->after('bidang');
            $table->text('map', 100)->nullable()->after('email');
            $table->string('gambar_banner', 255)->after('map');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pt', function (Blueprint $table) {
            //
            $table->dropColumn('no_telp');
            $table->dropColumn('bidang');
            $table->dropColumn('email');
            $table->dropColumn('map');
            $table->dropColumn('gambar_banner');
        });
    }
}
